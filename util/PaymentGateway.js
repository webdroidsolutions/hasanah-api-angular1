var BniHashing = require('./BniHashing.js');
var unirest = require('unirest');
// var dbConfig = require('../config/dbConfig.js');
var log = require('./logger.js').LOG;
require('x-date');


/* DOKU PRODUCTION*/
// var mallId = '';
// var sharedKey =  '';

/* DOKU DEVELOPMENT */
// var mallId = '4983';
// var sharedKey =  'e9Bg1WvpYD27';

/* BNI E-COLL PRODUCTION*/
var cid = '711';
var sck =  '41cff262481e8c8ce0e18d52b0557039';
var ecollhost = 'https://api.bni-ecollection.com/';

/* BNI E-COLL DEVELOPMENT */
// var cid = '299'; 
// var sck = 'dee5ef796901376f7aa562ee8191c0fa'; 
// var ecollhost = 'https://apibeta.bni-ecollection.com/';




function PaymentGateway() {
    // create reusable transporter object using the default SMTP transport
}
;

PaymentGateway.ecollCreate = function (req, trxId, callback) {
    try {
        var limit = new Date(+new Date() + 48 * 3600 * 1000);

//        var umroh_product_id = req.body.type_id;
//        var userdevice_id = req.body.userdevice_id;
//        var channel = req.body.channel;
        var basket = req.body.basket;
        var name = req.body.name;
        var email = req.body.email;
        var date = new Date().format('yyyy-mm-dd HH:MM:ss');
        var price = req.body.price;
        // var dp_price = req.body.dp_price;
        var payment_type = req.body.payment_type;

        var billPrice;
        // if(dp_price){
        //     billPrice = dp_price;
        // }else{




            billPrice = price;
            // billPrice = 10000;






        // }

        var data = {
            type: "createbilling",
            client_id: cid,
            trx_id: trxId,
            trx_amount: billPrice,
            billing_type: "c",
            customer_name: name,
            customer_email: email,
            customer_phone: "",
            description: "",//basket.split(";"),
            virtual_account: null,
            datetime_expired: limit.toISOString()
        };
        console.log(data);
        var hashed = BniHashing.hashData(data, cid, sck);

        /* USING UNIREST */
        unirest.post(ecollhost)
                .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
                .send({"client_id": cid, "data": hashed})
                .end(function (response) {

                    if (response.body.status !== '000') {
                        console.log("error" + JSON.stringify(response.body));
                        callback({status: "error", message: response.body.message}, null);
                    } else {
                        console.log("body:" + JSON.stringify(response.body));
                        var data_response = BniHashing.parseData(response.body.data, cid, sck);
                        console.log("parse:" + JSON.stringify(data_response));
                        callback(null, data_response);
                    }
                });
        /* USING UNIREST */





//        var args = {
//            data: {client_id: cid, data: hashed}, // data passed to REST method (only useful in POST, PUT or PATCH methods) 
////            path: {"id": 120}, // path substitution var 
////            parameters: {arg1: "hello", arg2: "world"}, // this is serialized as URL parameters 
//            headers: {"Content-type": "application/json"}, // request headers 
//            requestConfig: {
//                timeout: 1000, //request timeout in milliseconds 
//                noDelay: true, //Enable/disable the Nagle algorithm 
//                keepAlive: true, //Enable/disable keep-alive functionalityidle socket. 
//                keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent 
//            },
//            responseConfig: {
//                timeout: 1000 //response timeout 
//            }
//        };
//console.log("ARGS: " + args);
//        var args = {
////            path: {"id": 120},
////            parameters: {arg1: "hello", arg2: "world"},
//            headers: {"test-header": "client-api"},
////            data: "<xml><arg1>hello</arg1><arg2>world</arg2></xml>",
//            requestConfig: {
//                timeout: 1000, //request timeout in milliseconds 
//                noDelay: true, //Enable/disable the Nagle algorithm 
//                keepAlive: true, //Enable/disable keep-alive functionalityidle socket. 
//                keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent 
//            },
//            responseConfig: {
//                timeout: 1000 //response timeout 
//            }
//        };

//        var req = client.post("http://remote.site/rest/xml/${id}/method", args, function (data, response) {
//        var req = client.Client(devhost, args, function (data, response) {
//            // parsed response body as js object 
//            console.log(data);
//            // raw response 
//            console.log(response);
//
//            if (response.status !== '000') {
//
//            } else {
//                var data_response = BniHashing.parseData(response.data, cid, sck);
//                console.log(data_response);
//            }
//            res.send({status: "success", message: JSON.stringify(data_response)});
//        });

//        req.on('requestTimeout', function (req) {
//            console.log('request has expired');
//            req.abort();
//        });
//
//        req.on('responseTimeout', function (res) {
//            console.log('response has expired');
//
//        });
//
////it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts 
//        req.on('error', function (err) {
//            console.log('request error', err);
//        });

//==========================================================

//        var username = 'JonBob';
//        var password = '*****';
//        var apiKey = '*****';
//        var sessionId = null;
//        var deckId = '68DC5A20-EE4F-11E2-A00C-0858C0D5C2ED';
//
//        function performRequest(endpoint, method, data, success) {
//            var dataString = JSON.stringify(data);
//            var headers = {};
//
//            if (method == 'GET') {
//                endpoint += '?' + querystring.stringify(data);
//            }
//            else {
//                headers = {
//                    'Content-Type': 'application/json',
//                    'Content-Length': dataString.length
//                };
//            }
//            var options = {
//                host: host,
//                path: endpoint,
//                method: method,
//                headers: headers
//            };
//
//            var req = http.request(options, function (res) {
//                res.setEncoding('utf-8');
//
//                var responseString = '';
//
//                res.on('data', function (data) {
//                    responseString += data;
//                });
//
//                res.on('end', function () {
//                    console.log(responseString);
//                    var responseObject = JSON.parse(responseString);
//                    success(responseObject);
//                });
//            });
//
//            req.write(dataString);
//            req.end();
//        }

//====================================================





//        var model = require("../../models/" + docType);
//        model(docType);
//        model.paymentmode(req, function (err, result) {
//            if (err) {
//                res.status(400).send({status: "error", message: err.message});
//            } else {
//                res.render('pages/pay', {data: result});
//                // res.send(result);
//            }
//        });
    } catch (err) {
        log.error("srvPayRoute. : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = PaymentGateway;