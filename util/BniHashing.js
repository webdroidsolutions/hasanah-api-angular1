
function BniHashing() {
    // create reusable transporter object using the default SMTP transport
}
;


BniHashing.TIME_DIFF_LIMIT = function () {
    return 300;
}


//	static TIME_DIFF_LIMIT() { return 300; }

BniHashing.tsDiff = function (ts) {
    return Math.abs(ts - Math.round(+new Date() / 1e3)) <= this.TIME_DIFF_LIMIT();
}
//	static tsDiff(ts) { return Math.abs(ts - Math.round(+new Date() / 1e3)) <= this.TIME_DIFF_LIMIT(); }

BniHashing.str_pad = function (str, length, pad_char, pad_left) {
    while (str.length < length) {
        str = pad_left ? pad_char + str : str + pad_char;
    }
    return str;
}

//	static str_pad(str, length, pad_char, pad_left) {
//		while (str.length < length) {
//			str = pad_left ? pad_char + str : str + pad_char;
//		}
//		return str;
//	}

BniHashing.decrypt = function (str, sck) {
    var res = '';
    var strls = str.length;
    var strlk = sck.length;
    for (var i = 0; i < strls; i++) {
        var chr = str.substr(i, 1);
        var keychar = sck.substr((i % strlk) - 1, 1);
        chr = String.fromCharCode(((chr.charCodeAt() - keychar.charCodeAt()) + 256) % 128);
        res += chr;
    }
    return res;
}

//	static decrypt(str, sck) {
//		let res = '';
//		let strls = str.length;
//		let strlk = sck.length;
//		for (let i = 0; i < strls; i++) {
//			let chr = str.substr(i, 1);
//			let keychar = sck.substr((i % strlk) - 1, 1);
//			chr = String.fromCharCode(((chr.charCodeAt() - keychar.charCodeAt()) + 256) % 128);
//			res += chr;
//		}
//		return res;
//	}

BniHashing.encrypt = function (str, sck) {
    var res = '';
    var strls = str.length;
    var strlk = sck.length;
    for (var i = 0; i < strls; i++) {
        var chr = str.substr(i, 1);
        var keychar = sck.substr((i % strlk) - 1, 1);
        chr = String.fromCharCode((chr.charCodeAt() + keychar.charCodeAt()) % 128);
        res += chr;
    }
    return res;
}
//	static encrypt(str, sck) {
//		let res = '';
//		let strls = str.length;
//		let strlk = sck.length;
//		for (let i = 0; i < strls; i++) {
//			let chr = str.substr(i, 1);
//			let keychar = sck.substr((i % strlk) - 1, 1);
//			chr = String.fromCharCode((chr.charCodeAt() + keychar.charCodeAt()) % 128);
//			res += chr;
//		}
//		return res;
//	}

BniHashing.doubleDecrypt = function (str, cid, sck) {
    var res = Buffer.from(this.str_pad(str, Math.ceil(str.length / 4) * 4, '=', 0).replace(/-/g, '+').replace(/_/g, '/'), 'base64').toString('utf8');
    res = this.decrypt(res, cid);
    res = this.decrypt(res, sck);
    return res;
}
//	static doubleDecrypt(str, cid, sck) {
//		let res = Buffer.from(this.str_pad(str, Math.ceil(str.length / 4) * 4, '=', 0).replace(/-/g, '+').replace(/_/g, '/'), 'base64').toString('utf8');
//		res = this.decrypt(res, cid);
//		res = this.decrypt(res, sck);
//		return res;
//	}
BniHashing.doubleEncrypt = function (str, cid, sck) {
    var res = '';
    res = this.encrypt(str, cid);
    res = this.encrypt(res, sck);
    return Buffer.from(res, 'utf8').toString('base64').replace(/=+$/, '').replace(/\+/g, '-').replace(/\//g, '_');
}
//	static doubleEncrypt(str, cid, sck) {
//		let res = '';
//		res = this.encrypt(str, cid);
//		res = this.encrypt(res, sck);
//		return Buffer.from(res, 'utf8').toString('base64').replace(/=+$/, '').replace(/\+/g, '-').replace(/\//g, '_');
//	}
BniHashing.hashData = function (json_data, cid, sck) {
    return this.doubleEncrypt(('' + Math.round(+new Date() / 1e3)).split('').reverse().join('') + '.' + JSON.stringify(json_data), cid, sck);
}
//	static hashData(json_data, cid, sck) {
//		return this.doubleEncrypt(('' + Math.round(+new Date() / 1e3)).split('').reverse().join('') + '.' + JSON.stringify(json_data), cid, sck);
//	}
BniHashing.parseData = function (hashed_string, cid, sck) {
    var parsed_string = this.doubleDecrypt(hashed_string, cid, sck);
    var dot_pos = parsed_string.indexOf('.');
    if (dot_pos < 1)
        return null;
    var ts = parsed_string.substr(0, dot_pos);
    var data = parsed_string.substr(dot_pos + 1);
    if (this.tsDiff(ts.split('').reverse().join('')) === true) {
        return JSON.parse(data);
    }
    return null;
}
//	static parseData(hashed_string, cid, sck) {
//		let parsed_string = this.doubleDecrypt(hashed_string, cid, sck);
//		let dot_pos = parsed_string.indexOf('.');
//		if (dot_pos < 1)
//			return null;
//		let ts = parsed_string.substr(0, dot_pos);
//		let data = parsed_string.substr(dot_pos + 1);
//		if (this.tsDiff(ts.split('').reverse().join('')) === true) {
//			return JSON.parse(data);
//		}
//		return null;
//	}

//}



module.exports = BniHashing;
