var schedule = require('node-schedule');
var log = require('./logger.js').LOG;
 
var j = schedule.scheduleJob('4 * * * * *', function() {
    try {
        var token = require("../models/token");
        token("token");
        token.deleteAll(function(err, result) {
            console.log("SCHEDULER DELETE ALL TOKEN: ");
           if (err) {
               log.info("Delete token error : " + err);
           } else {
               log.info("Delete token success : " + result);
           }
        });
    } catch (err) {
        log.error("scheduleJob (TOKEN) : " + err);
    }
    
    try {
        var umroh = require("../models/umroh");
        umroh("umroh");
        umroh.disableOldUmroh(function(err, result) {
            console.log("SCHEDULER DISABLE OLD UMROH: ");
           if (err) {
               log.info("Disable umroh error : " + err);
           } else {
               log.info("Disable umroh success : " + result);
           }
        });
    } catch (err) {
        log.error("scheduleJob (UMROH) : " + err);
    }

    try {
        var umrohregister = require("../models/umrohregister");
        // umroh("umroh");
        umrohregister.deletePending(function(err, result) {
            console.log("SCHEDULER DELETE PENDING TRANSACATION: ");
            if (err) {
                log.info("Delete Pending Transaction Umroh error : " + err);
            } else {
                log.info("Delete Pending Transaction Umroh success : " + result);
            }
        });
    } catch (err) {
        console.log("ERR: "+err);
        log.error("scheduleJob (UMROHREGISTER) : " + err);
    }


    // try {
    //     var token = require("../models/token");
    //     token("token");
    //     token.test(function(err, result) {
    //         if (err) {
    //             console.log("ERR: "+err);
    //         } else {
    //             console.log("RESULT: "+JSON.stringify(result));
    //         }
    //     });
    // } catch (err) {
    //     console.log("ERR: "+err);
    //     log.error("test connection : " + err);
    // }
});