var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var cors = require("cors");
var log = require('./util/logger.js').LOG;

app.use(express.static(__dirname + '/public'));
app.use('/uploads', express.static(__dirname + '/uploads'));
app.use(express.static('./public/images/'));
app.use(cors());

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.set('view engine', 'ejs');
var scheduleJob = require("./util/scheduleJob.js");
var routes = require("./routes/routes.js")(app);

app.listen(8180);
log.info("Server started"); 