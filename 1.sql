
CREATE SCHEMA "Hasanah"
    AUTHORIZATION postgres;

CREATE TABLE "Hasanah".articlecategory
(
    id integer NOT NULL DEFAULT nextval('"Hasanah".articlecategory_id_seq'::regclass),
    img text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    CONSTRAINT articlecategory_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE "Hasanah".articlecategory
    OWNER to postgres;


CREATE TABLE "Hasanah".articlesubcategory
(
    id integer NOT NULL DEFAULT nextval('"Hasanah".articlesubcategory_id_seq'::regclass),
    articlecategoryid integer,
    img text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    CONSTRAINT articlesubcategory_pkey PRIMARY KEY (id),
    CONSTRAINT articlesubcategory_articlecategoryid_fkey FOREIGN KEY (id)
        REFERENCES "Hasanah".articlecategory (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE "Hasanah".articlesubcategory
    OWNER to postgres;

CREATE TABLE "Hasanah".article
(
    id integer NOT NULL DEFAULT nextval('"Hasanah".article_id_seq'::regclass),
    articlesubcategoryid integer,
    title text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    content text COLLATE pg_catalog."default",
    headline boolean,
    img text COLLATE pg_catalog."default",
    author text COLLATE pg_catalog."default",
    "time" reltime,
    status integer,
    CONSTRAINT article_pkey PRIMARY KEY (id),
    CONSTRAINT article_articlesubcategoryid_fkey FOREIGN KEY (id)
        REFERENCES "Hasanah".articlesubcategory (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE "Hasanah".article
    OWNER to postgres;