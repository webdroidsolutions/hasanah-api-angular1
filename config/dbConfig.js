var couchbase = require("couchbase");
require('x-date');
var queryFactory = couchbase.N1qlQuery;
var mysql = require('mysql');
const pg = require('pg');

var dbConfig = {
    dbName: "Hasanah"
};

dbConfig.getPostgreDb = function() {
    return pool = new pg.Pool({
        user: 'postgres',
        host: '127.0.0.1',
        database: 'Hasanah',
        password: '123456',
        port: '5432'});
    return pool;
};

    dbConfig.getDb = function() {
        const cluster = new couchbase.Cluster('couchbase://localhost');
        cluster.authenticate('root', 'H4s4n4h');
        const db = cluster.openBucket(dbConfig.dbName);
    //var db = (new couchbase.Cluster("http://localhost:8091")).openBucket(dbConfig.dbName);
    return db;
};

dbConfig.getStagingDb = function() {
    var db = (new couchbase.Cluster("http://localhost:8091")).openBucket(dbConfig.dbName);
    return db;
};

dbConfig.getImgPath= function(){
    //var basePath="http://116.206.196.234:8180/images/";
    var basePath="http://localhost:8180/";
    return basePath;
};

dbConfig.getTrxId= function(){
    var transidprefix = new Date().format('yymmddHHMMss');
    var string = dbConfig.getRandomString();
    var txId = "INV-" + transidprefix + "-" + string.toUpperCase();
    return txId;
};

dbConfig.getRandomString= function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
  return text;
};
dbConfig.getQueryFactory = function() {
    return queryFactory;
};

dbConfig.checkInsert = function(value) {
    if (value && (value+"")!='undefined' && (value+"").trim()!="")
        return "'"+value+"'";
    else
        return "DEFAULT";
}
dbConfig.checkUpdate = function(value, column) {
    if (!value || (value+"")=='undefined')
        return "DEFAULT";
    else if (value && (value+"")!='undefined' && (value+"").trim()!="")
        return "'"+value+"'";
    else
        return column;
}

dbConfig.getMysql = function() {
    var con = mysql.createConnection({
        // connectionLimit : 100, //important
        host: "localhost",
        user: "root",
        password: "H4s4n4h",//H4s4n4h
        database: "Hasanah"
    });
    console.log(con);
    return con;
};

module.exports = dbConfig;
