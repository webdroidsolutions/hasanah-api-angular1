var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
};

srvRoute.save = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.save(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvMessageRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getByCustomer = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getByCustomer(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvMessageRoute.getbycustomer : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getbyumroh = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getByUmroh(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvMessageRoute.getbyumroh : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getcountbycustomer = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getCountByCustomer(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getcountbycustomer : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getcountbyumroh = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getCountByUmroh(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getcountbyumroh : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};
module.exports = srvRoute;