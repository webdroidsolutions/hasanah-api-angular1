var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
}
;

srvRoute.paymentmode = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.paymentmode(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.render('pages/pay', {data: result});
                // res.send(result);
            }
        });
    } catch (err) {
        log.error("srvPayRoute. : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};


module.exports = srvRoute;