var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
};
    
srvRoute.uploadimg = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.uploadimg(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserUploadRoute.uploadimg : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.remove = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.remove(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserUploadRoute.uploadimg : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};
    

module.exports = srvRoute;