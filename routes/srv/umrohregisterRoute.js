var log = require('../../util/logger.js').LOG;
var docType = "";
var pg = require('../../util/PaymentGateway.js');
var dbConfig = require('../../config/dbConfig');

function srvRoute(clazz) {
    docType = clazz;
}
;

srvRoute.save = function (req, res) {
    // console.log(JSON.stringify(req.body));
    try {
        var va = null;
        var trxId = dbConfig.getTrxId();
        var model = require("../../models/" + docType);
        model(docType);

        if (req.body.channel === 'ecoll') {
            /* Create BNI VA */
            pg.ecollCreate(req, trxId, function (err, result) {
                if (err) {
                    console.log("ERR:" + err)
                    res.status(400).send({status: "error", message: err.message});
                } else {
                    va = result.virtual_account;
                    /* Create Order */
                    model.save(req, va, trxId, function (err, result) {
                        if (err) {
                            res.status(400).send({status: "error", message: err.message});
                        } else {
                            res.send(result);
                        }
                    });
                }
            });
        } else {
            /* Create Order Only */
            model.save(req, va, trxId, function (err, result) {
                if (err) {
                    res.status(400).send({status: "error", message: err.message});
                } else {
                    res.send(result);
                }
            });

        }



    } catch (err) {
        // log.error("srvUmrohregisterRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.edit = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.edit(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohregisterRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getbyid = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getById(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohregisterRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getByCustomer = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getByCustomer(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohregisterRoute.getByCustomer : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getall = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = srvRoute;