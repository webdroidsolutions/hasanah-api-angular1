var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
};

srvRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCityRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCityRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getallcity = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAllCity(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCityRoute.getallcity : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getcount = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getCount(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCityRoute.getcount : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.search = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.search(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCityRoute.search : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = srvRoute;