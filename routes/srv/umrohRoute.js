var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
};

srvRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getplane = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getPlane(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getplane : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getdeparturecity = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getDepartureCity(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getdeparturecity : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getactive = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getActive(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getactive : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getcount = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getCount(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.getcount : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.search = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.search(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUmrohRoute.search : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = srvRoute;