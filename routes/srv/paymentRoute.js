var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
}
;

srvRoute.redirect = function (req, res) {
    try {
        // var model = require("../../models/" + docType);
        // model(docType);
        // model.redirect(req, function (err, result) {
        //     if (err) {
        //         res.status(400).send({status: "error", message: err.message});
        //     } else {
        //         res.render('pages/dokuredirect', {data: req});
        //     }
        // });

        res.render('pages/dokuredirect', {data: req});
    } catch (err) {
        log.error("admPaymentRoute. : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.notify = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.notify(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {

                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPaymentRoute. : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.ecollnotif = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.ecollnotify(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPaymentRoute. : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.create = function (req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.create(req, function (err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {

                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvPaymentRoute. : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};


module.exports = srvRoute;