var log = require('../../util/logger.js').LOG;
var docType = "";

function admRoute(clazz) {
    docType = clazz;
};
    
admRoute.save = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.save(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSuggestionRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};
    
admRoute.delete = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.delete(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSuggestionRoute.delete : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSuggestionRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSuggestionRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.search = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.search(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSuggestionRoute.search : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = admRoute;