var log = require('../../util/logger.js').LOG;
var docType = "";

function admRoute(clazz) {
    docType = clazz;
};
    
admRoute.save = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.saveAgency(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admTravelAgencyRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admTravelAgencyRoute.getAll : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getbyname = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getByName(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admTravelAgencyRoute.getAll : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getProvince = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getProvince(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admTravelAgencyRoute.getProvince : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};


admRoute.edit = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.edit(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admTravelAgencyRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.delete = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.delete(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admRoleRoute.delete : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = admRoute;