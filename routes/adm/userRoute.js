var log = require('../../util/logger.js').LOG;
var docType = "";

function admRoute(clazz) {
    docType = clazz;
};


admRoute.login = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.login(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                var user=result;
                console.log(result);
                var pass = req.body.pass;
                if (result.pass===pass) {
                    var token = require("../../models/token");
                    token("token");
                    token.save(result, function(err, result) {
                        console.log(result);
                        user.sn=result.sn;
                        user.message=result.message;
                        // console.log(user);
                        // console.log(result);
                        if (err) {
                            res.status(400).send({status: "error", message: err.message});
                        } else {
                            res.send(user);
                        }
                    });
                } else {
                    res.status(400).send({status: "error", message: "Password tidak valid."});
                }
            }
        });
    } catch (err) {
        log.error("admUserRoute.login : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.logout = function(req, res) {
    try {
        var token = require("../../models/token");
        token("token");
        token.delete(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.logout : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.save = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.save(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.delete = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.delete(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.delete : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getemail = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getByEmail(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.getemail : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.search = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.search(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.search : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.forgot = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.forgotPassword(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admUserRoute.search : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.edit = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.edit(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = admRoute;