var log = require('../../util/logger.js').LOG;
var docType = "";

function admRoute(clazz) {
    docType = clazz;
};
    
admRoute.save = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.save(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPropertyfilingRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};
    
admRoute.delete = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.delete(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPropertyfilingRoute.delete : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPropertyfilingRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPropertyfilingRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getreport = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getReport(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPropertyfilingRoute.getreport : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.search = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.search(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admPropertyfilingRoute.search : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = admRoute;