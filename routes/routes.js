var log = require('../util/logger.js').LOG;
var appRouter = function(app) {
    
    app.post('/adm/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;
            console.log(req.body.sn);
            log.info("postAdm : /" + clazz + "/" + method);
            
            var routes = require("./adm/" + clazz + "Route.js");
            
            if (clazz==="user" && (method==="login" || method==="forgot")) {
                routes(clazz);
                routes[method](req, res);
            } else if (clazz==="util" && method==="upload") {
                routes(clazz);
                routes[method](req, res);
            } else if (clazz==="upload" && (method==="uploadimg" || method==="remove")) {
                routes(clazz);
                routes[method](req, res);
            } 
            else {
                var check = require("../util/checkToken.js");
                check.token(req, function(result) {
                    if (result===true) {
                        routes(clazz);
                        routes[method](req, res);
                    } else {
                        res.status(498).send({status: "error", message: "Access is not allowed."});
                    }
                });
            }
        } catch (err) {
            log.error("postAdm : " + err);
            res.status(404).send({status: "error", message: "Function not found."});
        }
    });
    
    app.get('/adm/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;
            
            log.info("getAdm : /" + clazz + "/" + method);
            var check = require("../util/checkToken.js");
            //console.log(req.body);
            check.token(req, function(result) {
                if (result===true) {
                    var routes = require("./adm/" + clazz + "Route.js");
                    routes(clazz);
                    routes[method](req, res);
                } else {
                    res.status(498).send({status: "error", message: "Access is not allowed."});
                }
            });
            //res.status(498).send({status: "error", message: "Access is not allowed."});
        } catch (err) {
            log.error("getAdm : " + err);
            res.status(404).send({status: "error", message: "Function not found."});
        }
    });
    
    
    app.post('/srv/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;

            log.info("postSrv : /" + clazz + "/" + method);
            var routes = require("./srv/" + clazz + "Route.js");
            routes(clazz);
            routes[method](req, res);
        } catch (err) {
            log.error("postSrv : " + err);
            res.status(404).send({status: "error", message: err});
        }
    });
    
    app.get('/srv/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;

            log.info("getSrv : /" + clazz + "/" + method);

            var routes = require("./srv/" + clazz + "Route.js");
            routes(clazz);
            routes[method](req, res);
        } catch (err) {
            log.error("getSrv : " + err);
            res.status(404).send({status: "error", message: err});
        }
    });

    // app.get('/uploads/:clazz/:file', function(req, res) {
    //         log.info("getSrv Image:");
    //         log.info(__dirname);
    //         res.sendFile(__dirname +'/'+req.params.clazz);
    // });
};

module.exports = appRouter;