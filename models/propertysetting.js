var log = require('../util/logger.js').LOG;
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var id = req.body.id;
    var title = req.body.title;
    var content = req.body.content;
    
    if (!id || !title || !content) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        pool.query(`UPDATE "Hasanah".propertysetting SET 
            email = '`+JSON.stringify(req.body.email).replace('[', '{').replace(']', '}')+`',
            content = `+dbConfig.checkUpdate(req.body.content, 'content')+`,
            title = `+dbConfig.checkUpdate(req.body.title, 'title')+`
            WHERE id = '`+id+`'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: id});
                }
            });
    }
};
model.getAll = function(req, callback) {
    pool.query(`SELECT *
        FROM "Hasanah".propertysetting 
        ORDER BY id`, (error, result) => {
            if (error) {
                callback(error, null);
            } else {
                if (result.rows.length == 0)
                    callback({status: "error", message: "Data tidak ditemukan."}, null);
                else
                    callback(null, result.rows[0]);
            }
        });
};

module.exports = model;