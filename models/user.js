var log = require('../util/logger.js').LOG;
var mail = require('../util/mail.js');
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var RoleModel = require('./role');

function model(docType) {
	documentType = docType;
};

var finalResult = [];

model.save = function (req, callback) {
	var id = req.body.id;
	var email = req.body.email.toLowerCase();
	var pass = req.body.pass;
	//var img = req.body.img;

	if (!email || !pass) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
		return;
	}
	if (req.body.user_type == 'TRAVEL ADMIN' && (!req.body.travelagent_id || req.body.travelagent_id == '')) {
		callback({status: "error", message: "Inputan tidak valid."}, null); 
		return;
	}
	if (!id) {
		pool.query(`SELECT *
			FROM "Hasanah".user 
			WHERE LOWER(email) = '`+email+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					if (result.rows.length > 0) {
						callback({status: "error", message: "Email telah terdaftar.", id: id}, null);
					} else {
						pool.query(`INSERT INTO "Hasanah".user 
							(travelagentid, name, email, pass, user_type, approve, description) 
							VALUES(`+dbConfig.checkInsert(req.body.travelagent_id)+`,
							`+dbConfig.checkInsert(req.body.name)+`, 
							`+dbConfig.checkInsert(email.toLowerCase())+`, 
							`+dbConfig.checkInsert(pass)+`, 
							`+dbConfig.checkInsert(req.body.user_type.toUpperCase())+`, 
							`+dbConfig.checkInsert(req.body.approve)+`, 
							`+dbConfig.checkInsert(req.body.description)+`)`, (error, result) => {
								if (error) {
									callback(error, null);
								} else {
									callback(null, {status: "success", message: "Data telah disimpan.", id: id});
								}
							});
					}
				}
			});
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".user 
			WHERE LOWER(email) = '`+email+`'
			AND id != '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					if (result.rows.length > 0) {
						callback({status: "error", message: "Email telah terdaftar.", id: id}, null);
					} else {
						pool.query(`UPDATE "Hasanah".user SET 
							travelagentid = `+dbConfig.checkUpdate(req.body.travelagent_id, 'travelagentid')+`,
							name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
							email = `+dbConfig.checkUpdate(email.toLowerCase(), 'email')+`,
							pass = `+dbConfig.checkUpdate(pass, 'pass')+`,
							user_type = `+dbConfig.checkUpdate(req.body.user_type.toUpperCase(), 'user_type')+`,
							approve = `+dbConfig.checkUpdate(req.body.approve, 'approve')+`,
							description = `+dbConfig.checkUpdate(req.body.description, 'description')+`
							WHERE id = '`+id+`'`, (error, result) => {
								if (error) {
									callback(error, null);
								} else {
									callback(null, {status: "success", message: "Data telah diubah.", id: id});
								}
							});
					}
				}
			});
	}
};

model.delete = function (req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".user 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function (req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".user 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function (req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".user 
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.login = function (req, callback) {
	var email = req.body.email.toLowerCase();
	var pass = req.body.pass;

	if (!email || !pass) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".user 
			WHERE email = '`+email+`'
			AND pass = '`+pass+`'
			LIMIT 1`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					if (result.rows.length <= 0) {
						callback({status: "error", message: "Email tidak terdaftar atau password salah."}, null);
						return;
					} else {
						(async () => {
							finalResult = [];
							start();

							async function start() {
								var user = result.rows[0];
								await getRole(" AND LOWER(name)='"+user.user_type.toLowerCase()+"'");
								await startMenus();

								user.menus = finalResult[0].menus;
								user.status = "success";
								callback(null, user);
							};
						})();
					}
				}
			});
	}
};

model.search = function (req, callback) {
	var key = req.body.key;

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".user 
			WHERE LOWER(description) like '%`+key.toLowerCase()+`%'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

model.forgotPassword = function (req, callback) {
	var email = req.body.email.toLowerCase();

	if (!email) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".user 
			WHERE email = '`+email+`'
			LIMIT 1`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					if (result.rows.length <= 0) {
						callback({status: "error", message: "Email tidak terdaftar."}, null);
					} else {
						mail.sendPassMail(result.rows);
						callback({status: "success", message: "Password has been sent to your mail."}, null);
					}
				}
			});
	}
};

// model.edit = function (req, callback) {
// 	var documentID = req.body.id;
// 	var newPass = req.body.pass;
// 	var user_type = req.body.user_type.toUpperCase();
// 	var newemail = req.body.email.toLowerCase();
// 	var newname = req.body.name;
// 	var newdescription = req.body.description;
// 	console.log(req.body);
//     //var myBucket = db.openBucket();
//     db.get(documentID, function (err, result) {
//     	if (err) {
//     		callback(err, null);
//     	} else {
//     		console.log(result);
//     		var obj = result.value;
//     		if (obj.pass == req.body.oldPass) {
//     			obj.pass = newPass;
//     			obj.user_type = user_type;
//     			obj.email = newemail;
//     			obj.name = newname;
//     			obj.description = newdescription;
//     			db.replace(documentID, obj, function (err, res) {
//     				if (err) {
//     					console.log('operation failed', err);
//     					callback(error, null);
//     				}
//     				else
//     				{
//     					console.log('success!', res);
//     					callback(null, {status: "success", message: "Data updated.", id: documentID});
//     				}
//     			});
//     		} else {
//     			callback("Maaf, password kamu salah", null);
//     		}
//     	}
//     });
// };

async function getRole(filter) {
	await pool.query(`SELECT *
		FROM "Hasanah".role 
		WHERE 1=1 `+filter+`
		ORDER BY id`)
	.then(result => {
		(async () => {
			for(var i = 0; i < result.rows.length; i++) {
				finalResult[i] = result.rows[i];
				finalResult[i]['menus'] = [];
			}
		})();
	}, error => {
		callback(error, null);
	});
}

async function startMenus() {
	for(var i = 0; i < finalResult.length; i++) {
		for(var j = 0; j < finalResult[i].menu.length; j++) {
			await getMenu(i, j);
		}
	}
}

async function getMenu(i, j) {
	await pool.query(`SELECT *
		FROM "Hasanah".menus 
		WHERE id = `+finalResult[i].menu[j]+`
		LIMIT 1`)
	.then(result => {
		(async () => {
			finalResult[i]['menus'][j] = result.rows[0];
		})();
	}, error => {
		callback(error, null);
	});
}

module.exports = model;
