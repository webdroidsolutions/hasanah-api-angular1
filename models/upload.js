var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var multer  = require('multer');
var documentType = "";
var path = require('path')
var imageDir = path.join(__dirname, '../uploads/passenger')

function model(docType) {
	documentType = docType;
};

var fileName;

var Storage = multer.diskStorage({
	destination: function (req, file, callback) {
		callback(null, imageDir);
	},
	filename: function (req, file, callback) {
		var arrayString = file.originalname.split(".");
		var fileNameExt = arrayString[arrayString.length - 1];
		fileName = Date.now()+"."+fileNameExt;
		callback(null, fileName);
	}
});

var upload = multer({ storage: Storage}).single("file");

model.uploadimg = function(req, callback) {
	upload(req, callback, function (err) { 
		if (err) { 
			callback(err,"Something went wrong!"); 
		}   
		callback(null,fileName); 
	}); 
};

model.remove = function(req,callback){
	var filePath=path.join(__dirname, '../uploads/passenger/')+req.body.name;
	if (fs.existsSync(filePath)) {
		fs.unlink(filePath, function(err){
			if(err){
				callback(null,err);
			} 
			callback(null,'file deleted successfully');
		})
	}else{
		callback(null,'no file found');
	}
};

module.exports = model;