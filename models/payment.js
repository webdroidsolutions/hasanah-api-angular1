var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sha1 = require('sha1');
var mail = require('../util/mail.js');
var BniHashing = require('../util/BniHashing.js');

/* DOKU PRODUCTION*/
var mallId = '2654';
var sharedKey =  'e9Bg1WvpYD27';
var dokuhost = 'https://pay.doku.com/Suite/Receive';

/* DOKU DEVELOPMENT */
// var mallId = '4983';
// var sharedKey =  'e9Bg1WvpYD27';

/* BNI E-COLL PRODUCTION*/
var cid = '711';
var sck =  '41cff262481e8c8ce0e18d52b0557039';

/* BNI E-COLL DEVELOPMENT */
// var cid = '299'; 
// var sck = 'dee5ef796901376f7aa562ee8191c0fa'; 

function model(docType) {
    documentType = docType;
}
;

model.redirect = function (req, callback) {

};

model.notify = function (req, callback) {
    console.log("notify --->");
    log.info(req.body);
    var transactionId = req.body.TRANSIDMERCHANT;
    var resultMsg = req.body.RESULTMSG;
    var verifyStatus = req.body.VERIFYSTATUS;
    var documentID;
    var dbname = dbConfig.dbName;
    var WORDS = sha1(req.body.AMOUNT + mallId + sharedKey + transactionId + resultMsg + verifyStatus);
    console.log(req.body.AMOUNT);
    console.log(mallId);
    console.log(sharedKey);
    console.log(transactionId);
    console.log(resultMsg);
    console.log(verifyStatus);
    console.log("gen:" + WORDS);
    console.log("req:" + req.body.WORDS);

    if (WORDS == req.body.WORDS) {
        console.log("words equality: same");
        if (req.body.RESULTMSG == 'SUCCESS') {
            pool.query(`UPDATE "Hasanah".umrohregister SET 
                payment_status = true, 
                status = 2
                WHERE order_id = '`+transactionId+`'
                RETURNING *`, (error, result) => {
                    if (error) {
                        console.log('operation failed', error);
                        //mail.sendOrderStatus(jsonData, function (result) { }); //jsonData undefined here
                    } else {
                        console.log('PAYMENT SUCCESS: ', transactionId);

                        pool.query(`UPDATE "Hasanah".umroh SET 
                            quota = quota - 1
                            WHERE id = '`+result.rows[0].typeid+`'`, (error1, result1) => {
                                if (error1) {
                                    console.log('operation failed', error1);
                                } else {
                                    console.log('DEDUCT TRAVEL QUOTA SUCCESS: ', transactionId);
                                }
                            });

                        mail.sendOrderStatus(result.rows[0], function (result) { });
                    }
                });
        } else {
            console.log('GET RESPONSE FROM DOKU PAYMENT FAILED:' + transactionId);
            //mail.sendOrderStatus(jsonData, function (result) { }); //jsonData undefined here
        }
        callback(null, "CONTINUE");
    }
    else {
        callback(null, "WORDS NOT MATCH");
    }
};

model.ecollnotify = function (req, callback) {
    log.info("ecoll notify --->");
    log.info(req.body);
    console.log('----------ecollnotify---------');

    var parse = BniHashing.parseData(req.body.data, cid, sck);
    //console.log("first pars: " + JSON.stringify(parse));
    /* FOR TESTING ONLY */
    if (!parse) {
        console.log("error" + JSON.stringify(req.body));
        console.log('GET RESPONSE FROM BNI E-COLL PAYMENT FAILED:');
    } else {
    //console.log("body:" + JSON.stringify(req.body));
    var parse = BniHashing.parseData(req.body.data, cid, sck);
    // console.log("pars:" + JSON.stringify(data_response));

    var transactionId = parse.trx_id;
    var virtual_account = parse.virtual_account;
    var customer_name = parse.customer_name;
    var trx_amount = parse.trx_amount; //total invoice
    var payment_amount = parse.payment_amount; //current paid
    var cumulative_payment_amount = parse.cumulative_payment_amount;
    var payment_ntb = parse.payment_ntb;
    var datetime_payment = parse.datetime_payment;

    pool.query(`UPDATE "Hasanah".umrohregister SET 
        payment_status = true, 
        status = 2, 
        va = `+dbConfig.checkUpdate(virtual_account, 'va')+`,
        payment_date = `+dbConfig.checkUpdate(datetime_payment, 'payment_date')+`,
        current_paid = `+dbConfig.checkUpdate(payment_amount, 'current_paid')+`,
        cumulative_paid = `+dbConfig.checkUpdate(cumulative_payment_amount, 'cumulative_paid')+`
        WHERE order_id = '`+transactionId+`'
        RETURNING *`, (error, result) => {
            if (error) {
                console.log('operation failed', error);
                callback(error, null);
            } else {
                console.log('PAYMENT SUCCESS: ', transactionId);

                pool.query(`UPDATE "Hasanah".umroh SET 
                    quota = quota - 1
                    WHERE id = '`+result.rows[0].typeid+`'`, (error1, result1) => {
                        if (error1) {
                            console.log('operation failed', error1);
                            callback(error1, null);
                        } else {
                            console.log('DEDUCT TRAVEL QUOTA SUCCESS: ', transactionId);
                        }
                    });

                mail.sendOrderStatus(result.rows[0], function (result) { });
                callback(null, {status: "000"});
            }
        });
};

model.create = function (req, callback) {
    console.log('-------------create------------');
    var data = req.body.data;
    var documentID;
    var counter = 0;
    var jsonData = data[counter];

    var addDocument = function (count) {
        jsonData = data[count];
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
        db.upsert(documentID, jsonData, function (error, result) {
            if (error) {
                callback(error, null);
            } else {
                if (++counter == data.length) {
                    callback(null, "successfully created");
                } else {
                    addDocument(counter);
                }

            }
        });
    }
    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
    }
    db.upsert(documentID, jsonData, function (error, result) {
        if (error) {
            callback(error, null);
        } else {
            addDocument(++counter);
        }
    });

};


module.exports = model;