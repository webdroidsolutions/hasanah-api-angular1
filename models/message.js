var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	if (!req.body.userdevice_id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`INSERT INTO "Hasanah".message 
			(travelagentid, userdeviceid, umrohid, customer_name, subject, description, create_date) 
			VALUES(
			`+dbConfig.checkInsert(req.body.travelagent_id)+`, 
			`+dbConfig.checkInsert(req.body.userdevice_id)+`, 
			`+dbConfig.checkInsert(req.body.umroh_id)+`, 
			`+dbConfig.checkInsert(req.body.customer_name)+`, 
			`+dbConfig.checkInsert(req.body.subject)+`, 
			`+dbConfig.checkInsert(req.body.description)+`,
			CURRENT_TIMESTAMP)`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah diubah."});
				}
			});
	};
};

model.edit = function(req, callback) {
	var id = req.body.id;

	if (!id || !req.body.reply) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	}
	else{
		pool.query(`UPDATE "Hasanah".message SET
			reply = `+dbConfig.checkUpdate(req.body.reply, 'reply')+`,
			reply_date = CURRENT_TIMESTAMP
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Message Sent Succesfully", id: id});
				}
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".message 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getByCustomer = function(req, callback) {
	var customer_id = req.body.userdevice_id;

	pool.query(`SELECT message.*, travelagency.name
		FROM "Hasanah".message 
		LEFT JOIN "Hasanah".travelagency ON travelagency.id = message.travelagentid
		WHERE message.userdeviceid = '`+customer_id+`'
		ORDER BY message.create_date DESC`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getByUmroh = function(req, callback) {
	var umroh_id = req.body.umroh_id;

	pool.query(`SELECT message.*, travelagency.name
		FROM "Hasanah".message 
		LEFT JOIN "Hasanah".travelagency ON travelagency.id = message.travelagentid
		WHERE message.umrohid = '`+umroh_id+`'
		ORDER BY message.create_date DESC`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getAll = function(req, callback) {
	var travelagent = req.body.travelagent_id;
	var filter = "";

	if (travelagent && travelagent !== "" && travelagent !== "undefined") {
		filter += " AND travelagentid='" + travelagent +"'";
	}

	pool.query(`SELECT *
		FROM "Hasanah".message 
		WHERE 1=1 `+filter+`
		ORDER BY create_date DESC`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCountByCustomer = function(req, callback) {
	var userdevice_id = req.body.userdevice_id;
	var filter = "";

	if (userdevice_id) {
		filter += " AND userdeviceid='" + userdevice_id +"';";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".message 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCountByUmroh = function(req, callback) {
	var umroh_id = req.body.umroh_id;
	var filter = "";

	if (umroh_id) {
		filter += " AND umrohid='" + umroh_id +"';";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".message 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;