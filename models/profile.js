var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var id = req.body.id;
    var email = req.body.email;
    var phone = req.body.phone;
        
    if (!id || !email || !phone || !username || !password) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var jsonData = {
            type: documentType,
            img: req.body.img,
            name: req.body.name,
            email: email,
            phone: phone,
        };
        
        db.upsert(id, jsonData, function(error, result) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, {status: "success", message: "Data telah disimpan.", id: id});
            }
        });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                var img = result.value.img;
                if (!img) {
                    callback(null, {status: "success", message: "Data telah dihapus."});
                } else {
                    db.remove(id, function (err, res) {
                        if (err) {
                            callback(err, null);
                        } else {
                            img = img.replace(appConfig.SERVER_ADDRESS, "");
                            fs.exists('./public/images/' + img, function(exists) {
                                if (exists) {
                                    fs.unlink('./public/images/' + img);
                                }
                            });
                            callback(null, {status: "success", message: "Data telah dihapus."});
                        }
                    });
                }
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var statement = "SELECT META(doc).id, img, name, email, phone "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY doc.id";
    
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, img, name, email, phone "
                + "FROM " + dbConfig.dbName + " AS doc " 
                + "WHERE name LIKE '%$1%' AND doc.type='" + documentType + "'";
        var query = queryFactory.fromString(statement);
        db.query(query, [key], function(err, result) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

module.exports = model;