var log = require('../util/logger.js').LOG;
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var id = req.body.id;
	var interval = req.body.interval;
	var repeat = req.body.repeat;
	
	if (!id || !interval || repeat==="") {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		if (id)  {
			pool.query(`UPDATE "Hasanah".homesetting SET 
				interval = `+dbConfig.checkUpdate(req.body.interval, 'interval')+`,
				repeat = `+dbConfig.checkUpdate(req.body.repeat, 'repeat')+`
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah diubah.", id: id});
					}
				});
		} else {
			pool.query(`INSERT INTO "Hasanah".homesetting 
				(interval, repeat) 
				VALUES(`+dbConfig.checkInsert(req.body.interval)+`,
				`+dbConfig.checkInsert(req.body.repeat)+`)`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah disimpan.", id: id});
					}
				});
		}
	}
};

model.getAll = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".homesetting 
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				if (result.rows.length == 0)
					callback({status: "error", message: "Data tidak ditemukan."}, null);
				else
					callback(null, result.rows);
			}
		});
};

module.exports = model;