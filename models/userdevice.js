var log = require('../util/logger.js').LOG;
var mail = require('../util/mail.js');
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var id = req.body.id;
	var email = req.body.email.toLowerCase();
	var phone = req.body.phone;
	var username = req.body.username.toLowerCase();
	var pass = req.body.pass;

	if (!email || !phone || !username || !pass) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
		return;
	}
	console.log(req.body);
	if (!id) {
		pool.query(`SELECT *
			FROM "Hasanah".userdevice
			WHERE LOWER(email) = '`+email+`'
			OR phone = '`+phone+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					if (result.rows.length > 0) {
						callback(null, {status: "error", message: "Email atau No Hp telah terdaftar."});
					} else {
						pool.query(`INSERT INTO "Hasanah".userdevice 
							(uuid, device, name, birthday, address, phone, email, username, pass) 
							VALUES(
							`+dbConfig.checkInsert(req.body.uuid)+`, 
							`+dbConfig.checkInsert(req.body.device)+`, 
							`+dbConfig.checkInsert(req.body.name)+`, 
							`+dbConfig.checkInsert(req.body.birthday)+`, 
							`+dbConfig.checkInsert(req.body.address)+`, 
							`+dbConfig.checkInsert(phone)+`, 
							`+dbConfig.checkInsert(email)+`, 
							`+dbConfig.checkInsert(username)+`, 
							`+dbConfig.checkInsert(pass)+`)`, (error, result) => {
								if (error) {
									callback(error, null);
								} else {
									callback(null, {status: "success", message: "Data telah disimpan.", id: id});
								}
							});
					}
				}
			});
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".userdevice
			WHERE (LOWER(email) = '`+email+`'
			OR phone = '`+phone+`')
			AND id != '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					if (result.rows.length > 0) {
						callback(null, {status: "error", message: "Email atau No Hp telah terdaftar."});
					} else {
						pool.query(`UPDATE "Hasanah".userdevice SET 
							uuid = `+dbConfig.checkUpdate(req.body.uuid, 'uuid')+`,
							device = `+dbConfig.checkUpdate(req.body.device, 'device')+`,
							name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
							birthday = `+dbConfig.checkUpdate(req.body.birthday, 'birthday')+`,
							address = `+dbConfig.checkUpdate(req.body.address, 'address')+`,
							phone = `+dbConfig.checkUpdate(phone, 'phone')+`,
							email = `+dbConfig.checkUpdate(email, 'email')+`,
							username = `+dbConfig.checkUpdate(username, 'username')+`,
							pass = `+dbConfig.checkUpdate(pass, 'pass')+`
							WHERE id = '`+id+`'`, (error, result) => {
								if (error) {
									callback(error, null);
								} else {
									callback(null, {status: "success", message: "Data telah diubah.", id: id});
								}
							});
					}
				}
			});
	}
};

model.changePassword = function(req, callback) {
	var id = req.body.id;
	var newPass = req.body.pass;
	var oldPass = req.body.old_pass;
	
	pool.query(`SELECT *
		FROM "Hasanah".userdevice 
		WHERE id = '`+id+`'
		AND pass = '`+oldPass+`'
		LIMIT 1`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				if (result.rows.length <= 0) {
					callback({status: "error", message: "Maaf, password kamu salah", id: id}, null);
					return;
				} else {
					pool.query(`UPDATE "Hasanah".userdevice SET 
						pass = '`+newPass+`'
						WHERE id = '`+id+`'`, (error, result) => {
							if (error) {
								callback(error, null);
							} else {
								callback(null, {status: "success", message: "Data updated.", id: id});
							}
						});
				}
			}
		});
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".userdevice 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getByUuid = function(req, callback) {
	var uuid = req.body.uuid;

	if (!uuid) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".userdevice 
			WHERE uuid = '`+uuid+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".userdevice 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}

	pool.query(`SELECT *
		FROM "Hasanah".userdevice 
		ORDER BY id
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCount = function(req, callback) {
	var key = req.body.key;
	var filter = "";

	if (key) {
		filter = filter + " AND LOWER(name) LIKE '%" + key.toLowerCase() + "%'";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".userdevice 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getReport = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".userdevice 
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.login = function(req, callback) {
	var username = req.body.username;
	var pass = req.body.pass;

	if (!username || !pass) {
		callback({status: "error", message: "Username dan password tidak ada"}, null);
		return;
	}

	pool.query(`SELECT *
		FROM "Hasanah".userdevice 
		WHERE LOWER(username) = '`+username.toLowerCase()+`'
		AND pass = '`+pass+`'
		LIMIT 1`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				if (result.rows.length <= 0) {
					callback({status: "error", message: "Username dan password tidak valid."}, null);
					return;
				} else {
					callback(null, result.rows[0]);
				}
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".userdevice 
			WHERE LOWER(name) like '%`+key.toLowerCase()+`%'
			LIMIT `+limit+` OFFSET `+offset, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

model.forgotPassword = function(req, callback) {
	var username = req.body.username;

	if (!username) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".userdevice 
			WHERE LOWER(username) = '`+username.toLowerCase()+`'
			LIMIT 1`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					if (result.rows.length <= 0) {
						callback({status: "error", message: "Username atau Email tidak terdaftar."}, null);
					} else {
						mail.sendPassMail(result.rows);
						callback({status: "success", message: "Password sudah terkirim ke email anda, silahkan cek spam bila belum muncul."}, null);
					}
				}
			});
	}
};

model.edit = function(req, callback) {
	var id = req.body.id;
	var address = req.body.address;
	var email = req.body.email;
	var name = req.body.name;
	var phone = req.body.phone;
	//var img = req.body.img;

	pool.query(`SELECT *
		FROM "Hasanah".userdevice
		WHERE (LOWER(email) = '`+email+`'
		OR phone = '`+phone+`')
		AND id != '`+id+`'`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				if (result.rows.length > 0) {
					callback(null, {status: "error", message: "Email atau No Hp telah terdaftar."});
				} else {
					pool.query(`UPDATE "Hasanah".userdevice SET 
						name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
						address = `+dbConfig.checkUpdate(req.body.address, 'address')+`,
						email = `+dbConfig.checkUpdate(email, 'email')+`,
						phone = `+dbConfig.checkUpdate(phone, 'phone')+`
						WHERE id = '`+id+`'`, (error, result) => {
							if (error) {
								callback(error, null);
							} else {
								callback(null, {status: "success", message: "Data updated.", id: id});
							}
						});
				}
			}
		});
};

module.exports = model;