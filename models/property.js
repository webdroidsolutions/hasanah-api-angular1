var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

function model(docType) {
	documentType = docType;
};

async function insertFileName(value) {
	fileNames.push(value);
}

async function resetFileName(value) {
	fileNames = [];
}

var firstFileName = null;
var fileNames = [];
var x=0;

model.save = function(req, callback) {
	var documentID = req.body.id;

	function saveImage(img, fileName) {
		var imgBuffer =  Buffer.from(img, 'base64');
		sharp(imgBuffer)
		.resize(360, 240)
		.toBuffer()
		.then(data => {
			(async () => {
				data = await imagemin.buffer(data, {
					plugins: [
					imageminMozjpeg(),
					imageminPngquant({quality: '40-60'})
					]
				});

				data =  Buffer.from(data).toString('base64');

				fs.writeFile(fileName, data, 'base64', function(err) {
					if (err) {
						callback({status: "error", message: "Inputan tidak valid."}, null);
						return;
					} 
				});
			})();
		})
		.catch(err => {
			callback({status: "error", message: "Inputan tidak invalid."}, null);
		});
	}

	async function uploadImage(x) {
		var fileName = "uploads/property/"+Date.now()+".png";
		if (req.body.img[x].substr(0,17) == "uploads/property/") //if update and image not changed
		{
			console.log('return: '+req.body.img[x]);
			fileName = req.body.img[x];
		}
		await insertFileName(fileName);

		if (req.body.img[x].substr(0,17) != "uploads/property/")
			await saveImage(req.body.img[x], fileName);
	};

	async function startImageUpload() {
		for (let x=0;x<req.body.img.length;x++) {
			await uploadImage(x);
		}
	};

	async function startUpload() {
		await resetFileName();
		await startImageUpload();

		var id = req.body.id;

		if (id)  {
			pool.query(`UPDATE "Hasanah".property SET 
				name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
				price = `+dbConfig.checkUpdate(req.body.price, 'price')+`,
				city = `+dbConfig.checkUpdate(req.body.city, 'city')+`,
				address = `+dbConfig.checkUpdate(req.body.address, 'address')+`,
				location = `+dbConfig.checkUpdate(req.body.location, 'location')+`,
				surface_area = `+dbConfig.checkUpdate(req.body.surface_area, 'surface_area')+`,
				building_area = `+dbConfig.checkUpdate(req.body.building_area, 'building_area')+`,
				auction_date = `+dbConfig.checkUpdate(req.body.auction_date, 'auction_date')+`,
				img = '`+JSON.stringify(fileNames).replace('[', '{').replace(']', '}')+`',
				info = `+dbConfig.checkUpdate(req.body.info, 'info')+`,
				status = `+dbConfig.checkUpdate(req.body.status, 'status')+`
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah diubah.", id: id});
					}
				});
		} else {
			pool.query(`INSERT INTO "Hasanah".property 
				(name, price, city, address, location, surface_area, building_area, auction_date, img, info, status) 
				VALUES(`+dbConfig.checkInsert(req.body.name)+`, 
				`+dbConfig.checkInsert(req.body.price)+`, 
				`+dbConfig.checkInsert(req.body.city)+`, 
				`+dbConfig.checkInsert(req.body.address)+`, 
				`+dbConfig.checkInsert(req.body.location)+`, 
				`+dbConfig.checkInsert(req.body.surface_area)+`, 
				`+dbConfig.checkInsert(req.body.building_area)+`, 
				`+dbConfig.checkInsert(req.body.auction_date)+`, 
				'`+JSON.stringify(fileNames).replace('[', '{').replace(']', '}')+`', 
				`+dbConfig.checkInsert(req.body.info)+`, 
				`+dbConfig.checkInsert(req.body.status)+`)`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah disimpan.", id: id});
					}
				});
		}
	};

	startUpload();
};

model.delete = function(req, callback) {
	var id = req.body.id;

	async function deleteImage(img) {
		await fs.exists(img, function(exists) {
			if (exists) {
				(async () => {
					await fs.unlink(img)
				})();
			}
		});
	};

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		(async () => {
			for(var img of req.body.old_img) {
				await deleteImage(img)
			}

			pool.query(`DELETE FROM "Hasanah".property 
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah dihapus."});
					}
				});
		})();
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".property 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	var status = req.body.status;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var filter = "";

	if (!limit) {
		limit = 5;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter = " AND status=" + status;
	}

	pool.query(`SELECT *
		FROM "Hasanah".property 
		WHERE 1=1 `+filter+`
		ORDER BY id
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCount = function(req, callback) {
	var city = req.body.city;
	var location = req.body.location;
	var minPrice = req.body.price_min;
	var maxPrice = req.body.price_max;
	var minSurfaceArea = req.body.surface_area_min;
	var maxSurfaceArea = req.body.surface_area_max;
	var minBuildingArea = req.body.building_area_min;
	var maxBuildingArea = req.body.building_area_max;
	var status = req.body.status;
	var filter = "";

	if (status) {
		filter = " AND status=" + status;
	}
	if (city) {
		filter += " AND LOWER(city) LIKE '%" + city.toLowercase() + "%'";
	}
	if (location) {
		filter += " AND LOWER(location) LIKE '%" + location.toLowerCase() + "%'";
	}
	if (minPrice && maxPrice) {
		filter += " AND (price>=" + minPrice + " AND price<=" + maxPrice + ")";
	}
	if (minSurfaceArea && maxSurfaceArea) {
		filter += " AND (surface_area>=" + minSurfaceArea + " AND surface_area<=" + maxSurfaceArea + ")";
	}
	if (minBuildingArea && maxBuildingArea) {
		filter += " AND (building_area>=" + minBuildingArea + " AND building_area<=" + maxBuildingArea + ")";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".property 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var city = req.body.city;
	var location = req.body.location;
	var minPrice = req.body.price_min;
	var maxPrice = req.body.price_max;
	var minSurfaceArea = req.body.surface_area_min;
	var maxSurfaceArea = req.body.surface_area_max;
	var minBuildingArea = req.body.building_area_min;
	var maxBuildingArea = req.body.building_area_max;
	var status = req.body.status;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var filter = "";

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter = " AND status=" + status;
	}
	if (city) {
		filter += " AND LOWER(city) LIKE '%" + city.toLowercase() + "%'";
	}
	if (location) {
		filter += " AND LOWER(location) LIKE '%" + location.toLowerCase() + "%'";
	}
	if (minPrice && maxPrice) {
		filter += " AND (price>=" + minPrice + " AND price<=" + maxPrice + ")";
	}
	if (minSurfaceArea && maxSurfaceArea) {
		filter += " AND (surface_area>=" + minSurfaceArea + " AND surface_area<=" + maxSurfaceArea + ")";
	}
	if (minBuildingArea && maxBuildingArea) {
		filter += " AND (building_area>=" + minBuildingArea + " AND building_area<=" + maxBuildingArea + ")";
	}

	pool.query(`SELECT *
		FROM "Hasanah".property 
		WHERE 1=1 `+filter+`
		ORDER BY id
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;