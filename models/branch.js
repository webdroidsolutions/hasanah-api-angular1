var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var documentID = req.body.id;

    if (req.body.img.substr(0,15) == "uploads/branch/") //if update and image not changed
    {
    	saveData(req, req.body.img, callback);
    }
    else
    {
        if (documentID) { //if update and image changed, delete old image
        	fs.exists(req.body.old_img, function(exists) {
        		if (exists) {
        			fs.unlink(req.body.old_img)
        		}
        	});
        }

        var imgBuffer =  Buffer.from(req.body.img, 'base64');
        sharp(imgBuffer)
        .resize(375, 200)
        .toBuffer()
        .then(data => {
        	(async () => {
        		data = await imagemin.buffer(data, {
        			plugins: [
        			imageminMozjpeg(),
        			imageminPngquant({quality: '40-60'})
        			]
        		});

        		data =  Buffer.from(data).toString('base64');

        		var fileName = "uploads/branch/"+Date.now()+".png";
                if (documentID) { //if update and image changed, keep old file name
                	fileName = req.body.old_img;
                }

                fs.writeFile(fileName, data, 'base64', function(err) {
                	if (err) {
                		callback({status: "error", message: "Inputan tidak valid."}, null);
                	}
                });
                
                saveData(req, fileName, callback);
            })();
        })
        .catch(err => {
        	callback({status: "error", message:"Inputan tidak invalid."}, null);
        });
    }

    function saveData(req, fileName, callback)
    {
    	var id = req.body.id;

    	if (id)  {
    		pool.query(`UPDATE "Hasanah".branch SET 
    			name = '`+req.body.name+`', 
    			address = `+dbConfig.checkUpdate(req.body.address, 'address')+`,
    			postcode = `+dbConfig.checkUpdate(req.body.postcode, 'postcode')+`,
    			province = `+dbConfig.checkUpdate(req.body.province, 'province')+`,
    			phone = `+dbConfig.checkUpdate(req.body.phone, 'phone')+`,
    			location = `+dbConfig.checkUpdate(req.body.location, 'location')+`,
    			img = '`+fileName+`',
    			status = `+dbConfig.checkUpdate(req.body.status, 'status')+`
    			WHERE id = '`+id+`'`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah diubah.", id: id});
    				}
    			});
    	} else {
    		pool.query(`INSERT INTO "Hasanah".branch 
    			(name, address, postcode, province, phone, location, img, status) 
    			VALUES('`+req.body.name+`', 
    			`+dbConfig.checkInsert(req.body.address)+`, 
    			`+dbConfig.checkInsert(req.body.postcode)+`, 
    			`+dbConfig.checkInsert(req.body.province)+`, 
    			`+dbConfig.checkInsert(req.body.phone)+`, 
    			`+dbConfig.checkInsert(req.body.location)+`, 
    			'`+fileName+`',  
    			`+dbConfig.checkInsert(req.body.status)+`)`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
    				}
    			});
    	}
    };
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".branch 
			WHERE id = '`+id+`'`, (error, result) => {
				fs.exists(req.body.old_img, function(exists) {
					if (exists) {
						fs.unlink(req.body.old_img)
					}
				});

				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".branch 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	var status = req.body.status;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var filter = "";

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter = " AND status=" + status;
	}

	pool.query(`SELECT *
		FROM "Hasanah".branch 
		WHERE 1=1 `+filter+`
		ORDER BY name
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getAllBranch = function(req, callback) {
	var filter = " AND status=1";

	pool.query(`SELECT id, name
		FROM "Hasanah".branch 
		WHERE 1=1 `+filter+`
		ORDER BY name`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCount = function(req, callback) {
	var key = req.body.key;
	var status = req.body.status;
	var filter = "";

	if (status) {
		filter = " AND status=" + status;
	}
	if (key) {
		key = key.toLowerCase();
		filter += " AND (LOWER(name) LIKE '%" + key + "%' OR LOWER(address) LIKE '%" + key 
		+ "%' OR LOWER(province) LIKE '%" + key + "%' OR LOWER(location) LIKE '%" + key + "%')";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".branch 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;
	var status = req.body.status;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var filter = "";

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter = " AND status=" + status;
	}    
	if (key) {
		key = key.toLowerCase();
		filter += " AND (LOWER(name) LIKE '%" + key + "%' OR LOWER(address) LIKE '%" + key 
		+ "%' OR LOWER(province) LIKE '%" + key + "%' OR LOWER(location) LIKE '%" + key + "%')";
	}

	pool.query(`SELECT *
		FROM "Hasanah".branch 
		WHERE 1=1 `+filter+`
		ORDER BY name
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;