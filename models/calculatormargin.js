var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var id = req.body.id;
	var description = req.body.description;

	if (!description) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		if (id)  {
			pool.query(`UPDATE "Hasanah".calculatormargin SET 
				countOfMonth = `+dbConfig.checkUpdate(req.body.href, 'countOfMonth')+`,
				percentage = `+dbConfig.checkUpdate(req.body.status, 'percentage')+`
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah diubah.", id: id});
					}
				});
		} else {
			pool.query(`INSERT INTO "Hasanah".calculatormargin
				(countOfMonth, percentage) 
				VALUES(`+dbConfig.checkInsert(req.body.href)+`,
				`+dbConfig.checkInsert(req.body.status)+`)`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah disimpan.", id: id});
					}
				});
		}
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".calculatormargin
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".calculatormargin 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".calculatormargin 
		ORDER BY countOfMonth`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getByMonth = function(req, callback) {
	var month = req.body.month;
	var filter = "";

	if (month) {
		filter = " AND LOWER(countOfMonth)=" + month.toLowerCase();
	}

	pool.query(`SELECT *
		FROM "Hasanah".calculatormargin 
		WHERE 1=1 `+filter+`
		ORDER BY countOfMonth`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;