var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var id = req.body.id;

	if (id)  {
		pool.query(`UPDATE "Hasanah".suggestion SET 
			name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
			phone = `+dbConfig.checkUpdate(req.body.phone, 'phone')+`,
			email = `+dbConfig.checkUpdate(req.body.email, 'email')+`,
			suggestion = `+dbConfig.checkUpdate(req.body.suggestion, 'suggestion')+`
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah diubah.", id: id});
				}
			});
	} else {
		pool.query(`INSERT INTO "Hasanah".suggestion 
			(name, phone, email, suggestion) 
			VALUES(`+dbConfig.checkInsert(req.body.name)+`, 
			`+dbConfig.checkInsert(req.body.phone)+`, 
			`+dbConfig.checkInsert(req.body.email)+`, 
			`+dbConfig.checkInsert(req.body.suggestion)+`)`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
				}
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".suggestion 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".suggestion 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".suggestion 
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".suggestion
			WHERE LOWER(suggestion) LIKE '%` + key.toLowerCase() + `%'
			ORDER BY id`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

module.exports = model;