var log = require('../util/logger.js').LOG;
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();


function model() {
};

model.save = function(documentID, jsonData, callback) {
    db.upsert(documentID, jsonData, function(error, result) {
        if (error) {
            callback(error, null);
        } else {
            callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
        }
    });
};

model.getById = function(documentID, callback) {
    db.get(documentID, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result.value);
        }
    });
};

module.exports = model;