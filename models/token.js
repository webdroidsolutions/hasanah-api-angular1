var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.test = function(callback) {
	pool.query(`SELECT *
		FROM "Hasanah".token 
		LIMIT 1`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows[0]);
			}
		});
};

model.save = function(user, callback) {
	pool.query(`INSERT INTO "Hasanah".token
		(email, user_type, exp) 
		VALUES(`+dbConfig.checkInsert(user.email)+`, 
		`+dbConfig.checkInsert(user.user_type)+`, 
		now() + interval '1 hour')
		RETURNING id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, {status: "success", message: "Login sukses.", name: user.name, 
					sn: result.rows[0].id, user_type: user.user_type});
			}
		});
};

model.delete = function(req, callback) {
	var id = req.body.sn;

	pool.query(`DELETE FROM "Hasanah".token 
		WHERE id = '`+id+`'`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, {status: "success", message: "Proses logout telah dilakukan."});
			}
		});
};

model.deleteAll = function(callback) {
	var now = new Date().format('yyyy-mm-dd HH:MM:ss');

	pool.query(`DELETE FROM "Hasanah".token
		WHERE exp < now()`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result);
			}
		});
};

model.getById = function(req, callback) {
	var id = req.body.sn;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".token
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

module.exports = model;