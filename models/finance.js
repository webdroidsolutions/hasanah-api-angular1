var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

var finalResult = [];

var FinanceID = 0;
var RateID = 0;
var TransactionStatus = true;
var TransactionError;
async function SetFinanceID(value) {
	FinanceID = value;
}
async function SetRateID(value) {
	RateID = value;
}
async function SetTransactionStatus(value, error) {
	TransactionStatus = value;
	TransactionError = error;
}

model.save = function(req, callback) {
	var id = req.body.id;

	if (id)  {
		(async () => {
			await SetFinanceID(id);

			await pool.query(`DELETE FROM "Hasanah".rate WHERE financeid = '`+id+`'`);

			await pool.query(`UPDATE "Hasanah".finance SET 
				name = `+dbConfig.checkUpdate(req.body.name, 'name')+`
				WHERE id = '`+id+`'`, (error, result) => {
					(async () => {
						if (error) {
							SetTransactionStatus(false, error);
							callback(error, null);
							return;
						} else {
							await InsertRates(req.body.rate);

							await Results();
						}
					})();
				});
		})();
	} else {
		(async () => {
			console.log(`INSERT INTO "Hasanah".finance (name) VALUES('`+req.body.name+`') RETURNING id`);
			await pool.query(`INSERT INTO "Hasanah".finance
				(name) 
				VALUES(`+dbConfig.checkInsert(req.body.name)+`)
				RETURNING id`, (error, result) => {
					(async () => {
						if (error) {
							SetTransactionStatus(false, error);
							callback(error, null);
							return;
						} else {
							await SetFinanceID(result.rows[0].id);
							await InsertRates(req.body.rate);

							await Results();
						}
					})();
				});
		})();
	}

	async function Results() {
		if (TransactionStatus) {
			if (id)
				callback(null, {status: "success", message: "Data telah diubah.", id: id});
			else
				callback(null, {status: "success", message: "Data telah disimpan.", id: id});
		} else {
			callback(TransactionError, null);
		}
	}

	async function InsertRates(rate) {
		for (var i=0; i<rate.length; i++) {
			await InsertRate(rate[i]);
		}
	}

	async function InsertMargins(margin) {
		for (var j=0; j<margin.length; j++) {
			await InsertMargin(margin[j]);
		}
	}

	async function InsertRate(rate) {
		console.log(`INSERT INTO "Hasanah".rate (financeid, job, dp) VALUES('`+FinanceID+`', '`+rate.job+`', '`+rate.dp+`') RETURNING id`);
		await pool.query(`INSERT INTO "Hasanah".rate 
			(financeid, job, dp) 
			VALUES(`+dbConfig.checkInsert(FinanceID)+`, 
			`+dbConfig.checkInsert(rate.job)+`, 
			`+dbConfig.checkInsert(rate.dp)+`)
			RETURNING id`, (error, result) => {
				(async () => {
					if (error) {
						SetTransactionStatus(false, error);
					} else {
						await SetRateID(result.rows[0].id);
						await InsertMargins(rate.margin);
					}
				})();
			});
	}

	async function InsertMargin(margin) {
		console.log(`INSERT INTO "Hasanah".margin (rateid, period, margin) VALUES('`+RateID+`', '`+margin.period+`', '`+margin.margin+`')`);
		await pool.query(`INSERT INTO "Hasanah".margin
			(rateid, period, margin) 
			VALUES(`+dbConfig.checkInsert(RateID)+`, 
			`+dbConfig.checkInsert(margin.period)+`, 
			`+dbConfig.checkInsert(margin.margin)+`)`, (error, result) => {
				if (error) {
					SetTransactionStatus(false, error);
				} 
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".finance 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		finalResult = [];
		start();

		async function start() {
			await getSurvey(" AND finance.id = '"+id+"'");
			await startUmroh();
			await startBranch();

			callback(null, finalResult);
		};
	}
};

model.getAll = function(req, callback) {
	finalResult = [];
	start();

	async function start() {
		await getUmrohFiling("");
		await startUmroh();
		await startBranch();

		callback(null, finalResult);
	};
};

model.search = function(req, callback) {
	var key = req.body.key;

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		finalResult = [];
		start();

		async function start() {
			await getUmrohFiling(" AND LOWER(name) like '"+key.toLowerCase()+"'");
			await startUmroh();
			await startBranch();

			callback(null, finalResult);
		};
	}
};

async function getUmrohFiling(filter) {
	await pool.query(`SELECT *
		FROM "Hasanah".finance 
		WHERE 1=1 `+filter+`
		ORDER BY id`)
	.then(result => {
		(async () => {
			for(var i = 0; i < result.rows.length; i++) {
				finalResult[i] = result.rows[i];
			}
		})();
	}, error => {
		callback(error, null);
	});
}

async function startUmroh() {
	for(var i = 0; i < finalResult.length; i++) {
		await getUmroh(i);
	}
}

async function getUmroh(i) {
	await pool.query(`SELECT *
		FROM "Hasanah".rate 
		WHERE financeid = `+finalResult[i]['id']+``)
	.then(result => {
		(async () => {
			finalResult[i]['rate'] = result.rows;
		})();
	}, error => {
		callback(error, null);
	});
}

async function startBranch() {
	for(var i = 0; i < finalResult.length; i++) {
		for(var j = 0; j < finalResult[i].rate.length; j++) {
			await getBranch(i, j);
		}
	}
}

async function getBranch(i, j) {
	await pool.query(`SELECT *
		FROM "Hasanah".margin 
		WHERE rateid = `+finalResult[i]['rate'][j]['id']+``)
	.then(result => {
		(async () => {
			finalResult[i]['rate'][j]['margin'] = result.rows;
		})();
	}, error => {
		callback(error, null);
	});
}

module.exports = model;