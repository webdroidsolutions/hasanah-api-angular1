var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var id = req.body.id;
    var question_id = req.body.id_question;
    var uuid = req.body.uuid;

    if (!id) {
        pool.query(`SELECT *
            FROM "Hasanah".surveyresult 
            WHERE uuid = '`+uuid+`'
            AND questionid = '`+question_id+`'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    if (result.rows.length > 0) {
                        callback({status: "error", message: "Maaf, anda telah mengisi survey ini sebelumnya.", id: id}, null);
                        return;
                    }
                }
            });
    }

    if (id)  {
        pool.query(`UPDATE "Hasanah".surveyresult SET 
            uuid = `+dbConfig.checkUpdate(uuid, 'uuid')+`,
            device = `+dbConfig.checkUpdate(req.body.device, 'device')+`,
            input_time = CURRENT_TIMESTAMP, 
            questionid = '`+question_id+`', 
            answer = `+dbConfig.checkUpdate(req.body.answer, 'answer')+`,
            WHERE id = '`+id+`'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: id});
                }
            });
    } else {
        pool.query(`INSERT INTO "Hasanah".surveyresult 
            (uuid, device, input_time, questionid, answer) 
            VALUES(`+dbConfig.checkInsert(uuid)+`, 
            `+dbConfig.checkInsert(req.body.device)+`, 
            CURRENT_TIMESTAMP, 
            '`+question_id+`', 
            `+dbConfig.checkInsert(req.body.answer)+`)`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah disimpan.", id: id});
                }
            });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;

    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        pool.query(`DELETE FROM "Hasanah".surveyresult 
            WHERE id = '`+id+`'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah dihapus."});
                }
            });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;

    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        pool.query(`SELECT *
            FROM "Hasanah".surveyresult 
            WHERE id = '`+id+`'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, result.rows[0]);
                }
            });
    }
};

model.getAll = function(req, callback) {
    pool.query(`SELECT *
        FROM "Hasanah".surveyresult 
        ORDER BY id`, (error, result) => {
            if (error) {
                callback(error, null);
            } else {
                callback(null, result.rows);
            }
        });
};

model.search = function(req, callback) {
    var key = req.body.key;
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        pool.query(`SELECT COUNT(1) as total
            FROM "Hasanah".surveyresult 
            WHERE LOWER(answer) LIKE '%` + key.toLowerCase() + `%'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, result.rows);
                }
            });
    }
};

module.exports = model;