var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var documentID = req.body.id;

    if (req.body.img.substr(0,18) == "uploads/quickmenu/") //if update and image not changed
    {
    	saveData(req, req.body.img, callback);
    }
    else
    {
        if (documentID) { //if update and image changed, delete old image
        	fs.exists(req.body.old_img, function(exists) {
        		if (exists) {
        			fs.unlink(req.body.old_img)
        		}
        	});
        }

        var imgBuffer =  Buffer.from(req.body.img, 'base64');
        sharp(imgBuffer)
        .resize(50, 50)
        .toBuffer()
        .then(data => {
        	(async () => {
        		data = await imagemin.buffer(data, {
        			plugins: [
        			imageminMozjpeg(),
        			imageminPngquant({quality: '40-60'})
        			]
        		});

        		data =  Buffer.from(data).toString('base64');

        		var fileName = "uploads/quickmenu/"+Date.now()+".png";
                if (documentID) { //if update and image changed, keep old file name
                	fileName = req.body.old_img;
                }

                fs.writeFile(fileName, data, 'base64', function(err) {
                	if (err) {
                		callback({status: "error", message: "Inputan tidak valid."}, null);
                	}
                });

                saveData(req, fileName, callback);
            })();
        })
        .catch(err => {
        	callback({status: "error", message: "Inputan tidak invalid."}, null);
        });
    }

    function saveData(req, fileName, callback)
    {
    	var id = req.body.id;

    	if (id)  {
    		pool.query(`UPDATE "Hasanah".quickmenu SET 
    			title = `+dbConfig.checkUpdate(req.body.title, 'title')+`,
    			description = `+dbConfig.checkUpdate(req.body.description, 'description')+`,
    			img = '`+fileName+`',
    			link = `+dbConfig.checkUpdate(req.body.link, 'link')+`
    			WHERE id = '`+id+`'`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah diubah.", id: id});
    				}
    			});
    	} else {
    		pool.query(`INSERT INTO "Hasanah".quickmenu 
    			(title, description, img, link) 
    			VALUES(`+dbConfig.checkInsert(req.body.title)+`, 
    			`+dbConfig.checkInsert(req.body.description)+`, 
    			'`+fileName+`', 
    			`+dbConfig.checkInsert(req.body.link)+`)`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
    				}
    			});
    	}
    };
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".quickmenu 
			WHERE id = '`+id+`'`, (error, result) => {
				fs.exists(req.body.old_img, function(exists) {
					if (exists) {
						fs.unlink(req.body.old_img)
					}
				});

				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".quickmenu 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".quickmenu
		ORDER BY id
		LIMIT 10`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".quickmenu 
			WHERE LOWER(title) like '%`+filter+`%'
			ORDER BY id`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

module.exports = model;