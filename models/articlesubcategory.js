var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var documentID = req.body.id;
	var id_article_category = req.body.id_article_category;
	var name = req.body.name;

	if (!id_article_category || !name) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
        if (req.body.img.substr(0,27) == "uploads/articlesubcategory/") //if update and image not changed
        {
        	saveData(req, req.body.img, callback);
        }
        else
        {
            if (documentID) { //if update and image changed, delete old image
            	fs.exists(req.body.old_img, function(exists) {
            		if (exists) {
            			fs.unlink(req.body.old_img)
            		}
            	});
            }

            var imgBuffer =  Buffer.from(req.body.img, 'base64');
            sharp(imgBuffer)
            .resize(180, 180)
            .toBuffer()
            .then(data => {
            	(async () => {
            		data = await imagemin.buffer(data, {
            			plugins: [
            			imageminMozjpeg(),
            			imageminPngquant({quality: '40-60'})
            			]
            		});

            		data =  Buffer.from(data).toString('base64');

            		var fileName = "uploads/articlesubcategory/"+Date.now()+".png";
                    if (documentID) { //if update and image changed, keep old file name
                    	fileName = req.body.old_img;
                    }

                    fs.writeFile(fileName, data, 'base64', function(err) {
                    	if (err) {
                    		callback({status: "error", message: "Inputan tidak valid."}, null);
                    	}
                    });

                    saveData(req, fileName, callback);
                })();
            })
            .catch(err => {
            	callback({status: "error", message: "Inputan tidak invalid."}, null);
            });
        }
    }

    function saveData(req, fileName, callback)
    {
    	var id = req.body.id;

    	if (id)  {
    		pool.query(`UPDATE "Hasanah".articlesubcategory SET 
    			articlecategoryid = '`+req.body.id_article_category+`', 
    			img = '`+fileName+`', 
    			name = '`+req.body.name+`', 
    			description = `+dbConfig.checkUpdate(req.body.description, 'description')+`
    			WHERE id = '`+id+`'`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah diubah.", id: id});
    				}
    			});
    	} else {
    		pool.query(`INSERT INTO "Hasanah".articlesubcategory 
    			(articlecategoryid, img, name, description) 
    			VALUES('`+req.body.id_article_category+`', '`+fileName+`', '`+req.body.name+`',  
    			`+dbConfig.checkInsert(req.body.description)+`)`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
    				}
    			});
    	}
    };
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".articlesubcategory 
			WHERE id = '`+id+`'`, (error, result) => {
				fs.exists(req.body.old_img, function(exists) {
					if (exists) {
						fs.unlink(req.body.old_img)
					}
				});

				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT articlesubcategory.id, articlecategory.id as id_article_category,
			articlecategory.name AS name_article_category,
			articlesubcategory.img, articlesubcategory.name, articlesubcategory.description
			FROM "Hasanah".articlesubcategory 
			INNER JOIN "Hasanah".articlecategory 
			ON articlecategory.id = articlesubcategory.articlecategoryid
			WHERE articlesubcategory.id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	pool.query(`SELECT articlesubcategory.id, articlecategory.id as id_article_category,
		articlecategory.name AS name_article_category,
		articlesubcategory.img, articlesubcategory.name, articlesubcategory.description
		FROM "Hasanah".articlesubcategory 
		INNER JOIN "Hasanah".articlecategory 
		ON articlecategory.id = articlesubcategory.articlecategoryid
		ORDER BY articlesubcategory.id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT articlesubcategory.id, articlecategory.id as id_article_category,
			articlecategory.name AS name_article_category,
			articlesubcategory.img, articlesubcategory.name, articlesubcategory.description
			FROM "Hasanah".articlesubcategory 
			INNER JOIN "Hasanah".articlecategory 
			ON articlecategory.id = articlesubcategory.articlecategoryid
			WHERE LOWER(articlesubcategory.description) like '%`+key.toLowerCase()+`%'
			ORDER BY articlesubcategory.id`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

model.getByIdArticleCategory = function(req, callback) {
	var id_article_category = req.body.id_article_category;

	if (!id_article_category) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT articlesubcategory.id, articlecategory.id as id_article_category,
			articlecategory.name AS name_article_category,
			articlesubcategory.img, articlesubcategory.name, articlesubcategory.description
			FROM "Hasanah".articlesubcategory 
			INNER JOIN "Hasanah".articlecategory 
			ON articlecategory.id = articlesubcategory.articlecategoryid
			WHERE articlecategory.id = '`+id_article_category+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

module.exports = model;