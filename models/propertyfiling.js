var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
var mail = require('../util/mail.js');
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
    documentType = docType;
};

var finalResult = [];

model.save = function(req, callback) {
    var id = req.body.id;
    var date = new Date().format('yyyy-mm-dd HH:MM:ss');

    if (id)  {
        //length = '`+req.body.length+`',
        pool.query(`UPDATE "Hasanah".propertyfilling SET 
            date = CURRENT_TIMESTAMP,
            name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
            address = `+dbConfig.checkUpdate(req.body.address, 'address')+`,
            birthday = `+dbConfig.checkUpdate(req.body.birthday, 'birthday')+`,
            business_field = `+dbConfig.checkUpdate(req.body.business_field, 'business_field')+`,
            city = `+dbConfig.checkUpdate(req.body.city, 'city')+`,
            company_type = `+dbConfig.checkUpdate(req.body.company_type, 'company_type')+`,
            contact_to = `+dbConfig.checkUpdate(req.body.contact_to, 'contact_to')+`,
            email = `+dbConfig.checkUpdate(req.body.email, 'email')+`,
            facility = `+dbConfig.checkUpdate(req.body.facility, 'facility')+`,
            gender = `+dbConfig.checkUpdate(req.body.gender, 'gender')+`,
            income = `+dbConfig.checkUpdate(req.body.income, 'income')+`,
            job = `+dbConfig.checkUpdate(req.body.job, 'job')+`,
            job_position = `+dbConfig.checkUpdate(req.body.job_position, 'job_position')+`,
            kecamatan = `+dbConfig.checkUpdate(req.body.kecamatan, 'kecamatan')+`,
            kelurahan = `+dbConfig.checkUpdate(req.body.kelurahan, 'kelurahan')+`,
            branchid = `+dbConfig.checkUpdate(req.body.nearest_branch_id, 'branchid')+`,
            office_name = `+dbConfig.checkUpdate(req.body.office_name, 'office_name')+`,
            office_phone = `+dbConfig.checkUpdate(req.body.office_phone, 'office_phone')+`,
            phone = `+dbConfig.checkUpdate(req.body.phone, 'phone')+`,
            postal_code = `+dbConfig.checkUpdate(req.body.postal_code, 'postal_code')+`,
            rt = `+dbConfig.checkUpdate(req.body.rt, 'rt')+`,
            rw = `+dbConfig.checkUpdate(req.body.rw, 'rw')+`,
            service_length = `+dbConfig.checkUpdate(req.body.service_length, 'service_length')+`,
            typeid = `+dbConfig.checkUpdate(req.body.type_id, 'typeid')+`,
            year_start_work = `+dbConfig.checkUpdate(req.body.year_start_work, 'year_start_work')+`,
            WHERE id = '`+id+`'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: id});
                }
            });
    } else {
        mail.send("propertysetting", function(result) {});

        pool.query(`INSERT INTO "Hasanah".propertyfilling 
            (date, name, address, birthday, business_field, city, company_type, contact_to, email, facility, gender, income, job, job_position, kecamatan,
            kelurahan, length,branchid,office_name,office_phone, phone, postal_code, rt, rw, service_length, typeid, year_start_work) 
            VALUES(CURRENT_TIMESTAMP, 
            `+dbConfig.checkInsert(req.body.name)+`,
            `+dbConfig.checkInsert(req.body.address)+`,
            `+dbConfig.checkInsert(req.body.birthday)+`,
            `+dbConfig.checkInsert(req.body.business_field)+`,
            `+dbConfig.checkInsert(req.body.city)+`,
            `+dbConfig.checkInsert(req.body.company_type)+`,
            `+dbConfig.checkInsert(req.body.contact_to)+`,
            `+dbConfig.checkInsert(req.body.email)+`,
            `+dbConfig.checkInsert(req.body.facility)+`,
            `+dbConfig.checkInsert(req.body.gender)+`,
            `+dbConfig.checkInsert(req.body.income)+`,
            `+dbConfig.checkInsert(req.body.job)+`,
            `+dbConfig.checkInsert(req.body.job_position)+`,
            `+dbConfig.checkInsert(req.body.kecamatan)+`,
            `+dbConfig.checkInsert(req.body.kelurahan)+`,
            DEFAULT,
            `+dbConfig.checkInsert(req.body.nearest_branch_id)+`,
            `+dbConfig.checkInsert(req.body.office_name)+`,
            `+dbConfig.checkInsert(req.body.office_phone)+`,
            `+dbConfig.checkInsert(req.body.phone)+`,
            `+dbConfig.checkInsert(req.body.postal_code)+`,
            `+dbConfig.checkInsert(req.body.rt)+`,
            `+dbConfig.checkInsert(req.body.rw)+`,
            `+dbConfig.checkInsert(req.body.service_length)+`,
            `+dbConfig.checkInsert(req.body.type_id)+`,
            `+dbConfig.checkInsert(req.body.year_start_work)+`)`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah disimpan.", id: id});
                }
            });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;

    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        pool.query(`DELETE FROM "Hasanah".propertyfilling 
            WHERE id = '`+id+`'`, (error, result) => {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah dihapus."});
                }
            });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;

    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        finalResult = [];
        start();

        async function start() {
            await getPropertyFiling(" AND propertyfilling.id = '"+id+"'");
            await startProperty();
            await startBranch();

            callback(null, finalResult);
        };
    }
};

model.getAll = function(req, callback) { 
    finalResult = [];
    start();

    async function start() {
        await getPropertyFiling("");
        await startProperty();
        await startBranch();

        callback(null, finalResult);
    };
};

model.getReport = function(req, callback) {
    var dateStart = req.body.date_start;
    var dateEnd = req.body.date_end;

    var filter = "";
    if (dateStart && dateEnd) {
        filter += " AND propertyfilling.date BETWEEN '" + dateStart + "' AND '" + dateEnd + "'";
    }

    finalResult = [];
    start();

    async function start() {
        await getPropertyFiling(filter);
        await startProperty();
        await startBranch();

        callback(null, finalResult);
    };
};

model.search = function(req, callback) {
    var key = req.body.key;

    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        finalResult = [];
        start();

        async function start() {
            await getPropertyFiling(" AND LOWER(propertyfilling.name) LIKE '%"+key.toLowerCase()+"%'");
            await startProperty();
            await startBranch();

            callback(null, finalResult);
        };
    }
};

async function getPropertyFiling(filter) {
    await pool.query(`SELECT propertyfilling.*
        FROM "Hasanah".propertyfilling 
        WHERE 1=1 `+filter+`
        ORDER BY propertyfilling.id`)
    .then(result => {
        (async () => {
            for(var i = 0; i < result.rows.length; i++) {
                finalResult[i] = {}
                finalResult[i]['propertyfiling'] = result.rows[i];
            }
        })();
    }, error => {
        callback(error, null);
    });
}

async function startProperty() {
    for(var i = 0; i < finalResult.length; i++) {
        await getProperty(i);
    }
}

async function getProperty(i) {
    await pool.query(`SELECT *
        FROM "Hasanah".property 
        WHERE id = `+finalResult[i]['propertyfiling']['typeid']+`
        LIMIT 1`)
    .then(result => {
        (async () => {
            finalResult[i]['property'] = result.rows[0];
        })();
    }, error => {
        callback(error, null);
    });
}

async function startBranch() {
    for(var i = 0; i < finalResult.length; i++) {
        await getBranch(i);
    }
}

async function getBranch(i) {
    await pool.query(`SELECT *
        FROM "Hasanah".branch 
        WHERE id = `+finalResult[i]['propertyfiling']['branchid']+`
        LIMIT 1`)
    .then(result => {
        (async () => {
            finalResult[i]['branch'] = result.rows[0];
        })();
    }, error => {
        callback(error, null);
    });
}

module.exports = model;