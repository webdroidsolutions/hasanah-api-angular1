var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var documentID = req.body.id;
	var id_article_sub_category = req.body.id_article_sub_category;
	var title = req.body.title;

	if (!id_article_sub_category || !title) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
        if (req.body.img.substr(0,16) == "uploads/article/") //if update and image not changed
        {
        	saveData(req, req.body.img, callback);
        }
        else
        {
            if (documentID) { //if update and image changed, delete old image
            	fs.exists(req.body.old_img, function(exists) {
            		if (exists) {
            			fs.unlink(req.body.old_img)
            		}
            	});
            }

            var imgBuffer =  Buffer.from(req.body.img, 'base64');
            sharp(imgBuffer)
            .resize(375, 200)
            .toBuffer()
            .then(data => {
            	(async () => {
            		data = await imagemin.buffer(data, {
            			plugins: [
            			imageminMozjpeg(),
            			imageminPngquant({quality: '40-60'})
            			]
            		});

            		data =  Buffer.from(data).toString('base64');

            		var fileName = "uploads/article/"+Date.now()+".png";
                    if (documentID) { //if update and image changed, keep old file name
                    	fileName = req.body.old_img;
                    }

                    fs.writeFile(fileName, data, 'base64', function(err) {
                    	if (err) {
                    		callback({status: "error", message: "Inputan tidak valid."}, null);
                    	}
                    });

                    saveData(req, fileName, callback);
                })();
            })
            .catch(err => {
            	callback({status: "error", message: "Inputan tidak invalid."}, null);
            });
        }
    }

    function saveData(req, fileName, callback)
    {
    	var id = req.body.id;

    	if (id)  {
    		pool.query(`UPDATE "Hasanah".article SET 
    			articlesubcategoryid = '`+req.body.id_article_sub_category+`', 
    			title = `+dbConfig.checkUpdate(req.body.title, 'title')+`,
    			description = `+dbConfig.checkUpdate(req.body.description, 'description')+`,
    			content = `+dbConfig.checkUpdate(req.body.content, 'content')+`,
    			headline = `+dbConfig.checkUpdate(req.body.headline, 'headline')+`,
    			img = '`+fileName+`',
    			author = `+dbConfig.checkUpdate(req.body.author, 'author')+`,
    			time = `+dbConfig.checkUpdate(req.body.time, 'time')+`,
    			status = `+dbConfig.checkUpdate(req.body.status, 'status')+`
    			WHERE id = '`+id+`'`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah diubah.", id: id});
    				}
    			});
    	} else {
    		pool.query(`INSERT INTO "Hasanah".article 
    			(articlesubcategoryid, title, description, content, headline, img, author, time, status) 
    			VALUES('`+req.body.id_article_sub_category+`', 
    			`+dbConfig.checkInsert(req.body.title)+`, 
    			`+dbConfig.checkInsert(req.body.description)+`, 
    			`+dbConfig.checkInsert(req.body.content)+`, 
    			`+dbConfig.checkInsert(req.body.headline)+`, 
    			'`+fileName+`', 
    			`+dbConfig.checkInsert(req.body.author)+`, 
    			`+dbConfig.checkInsert(req.body.time)+`, 
    			`+dbConfig.checkInsert(req.body.status)+`)`, (error, result) => {
    				if (error) {
    					callback(error, null);
    				} else {
    					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
    				}
    			});
    	}
    };
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".article 
			WHERE id = '`+id+`'`, (error, result) => {
				fs.exists(req.body.old_img, function(exists) {
					if (exists) {
						fs.unlink(req.body.old_img)
					}
				});

				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT article.id, articlesubcategory.id as id_article_sub_category,
			articlesubcategory.name AS name_article_sub_category,
			article.headline, article.img, article.title, article.time, article.author, 
			article.description, article.content, article.status
			FROM "Hasanah".article 
			INNER JOIN "Hasanah".articlesubcategory 
			ON articlesubcategory.id = article.articlesubcategoryid
			WHERE article.id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	var status = req.body.status;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var filter = "";

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter = " AND article.status=" + status;
	}

	pool.query(`SELECT article.id, articlesubcategory.id as id_article_sub_category,
		articlesubcategory.name AS name_article_sub_category,
		article.headline, article.img, article.title, article.time, article.author, 
		article.description, article.content, article.status
		FROM "Hasanah".article 
		INNER JOIN "Hasanah".articlesubcategory 
		ON articlesubcategory.id = article.articlesubcategoryid
		WHERE 1=1 `+filter+`
		ORDER BY article.id
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCount = function(req, callback) {
	var key = req.body.key;
	var status = req.body.status;
	var filter = "";

	if (status) {
		filter = " AND article.status=" + status;
	}
	if (key) {
		key = key.toLowerCase();
		filter += " AND (LOWER(article.title) LIKE '%" + key + "%' OR LOWER(article.description) LIKE '%" + key + "%')";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".article 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getByIdArticleSubCategory = function(req, callback) {
	var id_article_sub_category = req.body.id_article_sub_category;
	var status = req.body.status;
	var filter = "";

	if (status) {
		filter = " AND article.status=" + status;
	}

	pool.query(`SELECT article.id, articlesubcategory.id as id_article_sub_category,
		articlesubcategory.name AS name_article_sub_category,
		article.headline, article.img, article.title, article.time, article.author, 
		article.description, article.content, article.status
		FROM "Hasanah".article 
		INNER JOIN "Hasanah".articlesubcategory 
		ON articlesubcategory.id = article.articlesubcategoryid
		WHERE article.articlesubcategoryid = '`+id_article_sub_category+`'
		ORDER BY article.id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getHeadline = function(req, callback) {
	var status = req.body.status;
	var filter = "";

	if (status) {
		filter = " AND article.status=" + status;
	}

	pool.query(`SELECT article.id, articlesubcategory.id as id_article_sub_category,
		articlesubcategory.name AS name_article_sub_category,
		article.headline, article.img, article.title, article.time, article.author, 
		article.description, article.content, article.status
		FROM "Hasanah".article 
		INNER JOIN "Hasanah".articlesubcategory 
		ON articlesubcategory.id = article.articlesubcategoryid
		WHERE article.headline = true and article.status = 1
		ORDER BY article.id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;
	var status = req.body.status;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var filter = "";

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter = " AND status=" + status;
	}
	if (key) {
		key = key.toLowerCase();
		filter += " AND (LOWER(article.title) LIKE '%" + key + "%' OR LOWER(article.description) LIKE '%" + key + "%')";
	}

	pool.query(`SELECT article.id, articlesubcategory.id as id_article_sub_category,
		articlesubcategory.name AS name_article_sub_category,
		article.headline, article.img, article.title, article.time, article.author, 
		article.description, article.content, article.status
		FROM "Hasanah".article 
		INNER JOIN "Hasanah".articlesubcategory 
		ON articlesubcategory.id = article.articlesubcategoryid
		WHERE 1=1 `+filter+`
		ORDER BY article.id
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;