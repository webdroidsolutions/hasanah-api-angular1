var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

var finalResult = [];

var SurveyID = 0;
var TransactionStatus = true;
var TransactionError;
async function SetSurveyID(value) {
	SurveyID = value;
}
async function SetTransactionStatus(value, error) {
	TransactionStatus = value;
	TransactionError = error;
}

model.save = function(req, callback) {
	var id = req.body.id;
	
	if (id)  {
		(async () => {
			console.log(`UPDATE "Hasanah".survey SET question = '`+req.body.question+`', question_type = '`+req.body.question_type+`', "default" = '`+req.body.default+`', max = '`+req.body.max+`', status = '`+req.body.stauts+`' WHERE id = '`+id+`'`);
			await SetSurveyID(id);

			await pool.query(`DELETE FROM "Hasanah".survey_options WHERE surveyid = '`+id+`'`);

			await pool.query(`UPDATE "Hasanah".survey SET 
				question = `+dbConfig.checkUpdate(req.body.question, 'question')+`,
				question_type = `+dbConfig.checkUpdate(req.body.question_type, 'question_type')+`,
				"default" = '`+req.body.default+`', 
				max = `+dbConfig.checkUpdate(req.body.max, 'max')+`,
				status = `+dbConfig.checkUpdate(req.body.status, 'status')+`
				WHERE id = '`+id+`'`, (error, result) => {
					(async () => {
						if (error) {
							SetTransactionStatus(false, error);
							callback(error, null);
							return;
						} else {
							await InsertOptions(req.body.options);

							await Results();
						}
					})();
				});
		})();
	} else {
		(async () => {
			await pool.query(`INSERT INTO "Hasanah".survey
				(question, question_type, "default", max, status) 
				VALUES(`+dbConfig.checkInsert(req.body.question)+`, 
				`+dbConfig.checkInsert(req.body.question_type)+`, 
				`+dbConfig.checkInsert(req.body.default)+`, 
				`+dbConfig.checkInsert(req.body.max)+`, 
				`+dbConfig.checkInsert(req.body.status)+`)
				RETURNING id`, (error, result) => {
					(async () => {
						if (error) {
							SetTransactionStatus(false, error);
							callback(error, null);
							return;
						} else {
							await SetSurveyID(result.rows[0].id);
							await InsertOptions(req.body.options);

							await Results();
						}
					})();
				});
		})();
	}

	async function Results() {
		if (TransactionStatus) {
			if (id)
				callback(null, {status: "success", message: "Data telah diubah.", id: id});
			else
				callback(null, {status: "success", message: "Data telah disimpan.", id: id});
		} else {
			callback(TransactionError, null);
		}
	}

	async function InsertOptions(options) {
		for (var i=0; i<options.length; i++) {
			await InsertOption(options[i]);
		}
	}

	async function InsertOption(option) {
		console.log(`INSERT INTO "Hasanah".survey_options (surveyid, value, text) VALUES('`+SurveyID+`', '`+option.value+`', '`+option.text+`')`);
		await pool.query(`INSERT INTO "Hasanah".survey_options
			(surveyid, value, text) 
			VALUES('`+SurveyID+`', 
			`+dbConfig.checkInsert(option.value)+`, 
			`+dbConfig.checkInsert(option.text)+`)`, (error, result) => {
				if (error) {
					SetTransactionStatus(false, error);
				} 
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".survey 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		finalResult = [];
		start();

		async function start() {
			await getSurvey(" AND survey.id = '"+id+"'");
			await startOptions();

			callback(null, finalResult);
		};
	}
};

model.getAll = function(req, callback) {
	var id = req.body.id;
	var status = req.body.status;
	var filter = "";
	
	if (req.body.status) {
		filter = " AND status=" + status;
	}
	finalResult = [];
	start();

	async function start() {
		await getSurvey(filter);
		await startOptions();

		callback(null, finalResult);
	};
};

model.search = function(req, callback) {
	var key = req.body.key;
	var status = req.body.status;
	var filter = "";
	
	if (status) {
		filter = " AND status=" + status;
	}
	filter += " AND LOWER(question) like '%" + key.toLowerCase()+"%'";

	finalResult = [];
	start();

	async function start() {
		await getSurvey(filter);
		await startOptions();

		callback(null, finalResult);
	};
};

async function getSurvey(filter) {
	await pool.query(`SELECT *
		FROM "Hasanah".survey 
		WHERE 1=1 `+filter+`
		ORDER BY id`)
	.then(result => {
		(async () => {
			for(var i = 0; i < result.rows.length; i++) {
				finalResult[i] = result.rows[i];
				finalResult[i]['options'] = {};
			}
		})();
	}, error => {
		callback(error, null);
	});
}

async function startOptions() {
	for(var i = 0; i < finalResult.length; i++) {
		await getOption(i);
	}
}

async function getOption(i) {
	await pool.query(`SELECT *
		FROM "Hasanah".survey_options 
		WHERE surveyid = `+finalResult[i]['id'])
	.then(result => {
		(async () => {
			finalResult[i]['options'] = result.rows;
		})();
	}, error => {
		callback(error, null);
	});
}

module.exports = model;