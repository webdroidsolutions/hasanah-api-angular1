var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

var finalResult = [];

model.save = function(req, callback) {
	var id = req.body.id;
	var name = req.body.name;

	if (!name) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		if (!id) {
			pool.query(`SELECT *
				FROM "Hasanah".role 
				WHERE LOWER(name) = '`+req.body.name.toLowerCase()+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						if (result.rows.length > 0) {
							callback({status: "success", message: "Name telah terdaftar."}, null);
							return;
						}
					}
				});
		}

		if (id)  {
			pool.query(`UPDATE "Hasanah".role SET 
				name = `+dbConfig.checkUpdate(req.body.name, 'name')+`
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah diubah.", id: id});
					}
				});
		} else {
			pool.query(`INSERT INTO "Hasanah".role
				(name, menu) 
				VALUES(`+dbConfig.checkInsert(req.body.name)+`, '{}')`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah disimpan.", id: id});
					}
				});
		}
	};
};

model.getAll = function(req, callback) {
	finalResult = [];
	start();

	async function start() {
		await getRole("");
		await startMenus();
		
		for(var i = 0; i < finalResult.length; i++) {
			delete finalResult[i]['menu'];
		}

		callback(null, finalResult);
	};
};

model.getByName = function(req, callback) {
	var name = req.body.name;

	finalResult = [];
	start();

	async function start() {
		await getRole(" AND LOWER(name)='"+name.toLowerCase()+"'");
		await startMenus();
		for(var i = 0; i < finalResult.length; i++) {
			delete finalResult[i]['menu'];
		}

		callback(null, finalResult);
	};
};

model.getMenus = function(req, callback) {
	finalResult = [];
	finalResult[0] = {};
	finalResult[0]['menus'] = {};
	
	pool.query(`SELECT *
		FROM "Hasanah".menus
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				for(var i = 0; i < result.rows.length; i++) {
					finalResult[0]['menus'][i] = result.rows[i];
				}
				callback(null, finalResult);
			}
		});
};

model.edit = function(req, callback) {
	var id = req.body.id;
	var newMenu = req.body.menus;

	if (!id || !newMenu) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		var Menus = [];
		for(var i = 0; i < req.body.menus.length; i++) {
			Menus.push(req.body.menus[i].id);
		}

		pool.query(`UPDATE "Hasanah".role SET 
			menu = '`+JSON.stringify(Menus).replace('[', '{').replace(']', '}')+`'
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data updated.", id: id});
				}
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".role 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

async function getRole(filter) {
	await pool.query(`SELECT *
		FROM "Hasanah".role 
		WHERE 1=1 `+filter+`
		ORDER BY id`)
	.then(result => {
		(async () => {
			for(var i = 0; i < result.rows.length; i++) {
				finalResult[i] = result.rows[i];
				finalResult[i]['menus'] = [];
			}
		})();
	}, error => {
		callback(error, null);
	});
}

async function startMenus() {
	for(var i = 0; i < finalResult.length; i++) {
		for(var j = 0; j < finalResult[i].menu.length; j++) {
			await getMenu(i, j);
		}
	}
}

async function getMenu(i, j) {
	await pool.query(`SELECT *
		FROM "Hasanah".menus 
		WHERE id = `+finalResult[i].menu[j]+`
		LIMIT 1`)
	.then(result => {
		(async () => {
			finalResult[i]['menus'][j] = result.rows[0];
		})();
	}, error => {
		callback(error, null);
	});
}

module.exports = model;