var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sha1 = require('sha1');

function model(docType) {
	documentType = docType;
};

model.paymentmode = function(req, callback) {
	var orderId = req.query.id;
	var paymentmode = req.query.mode;
	var mallId = "4983";
	var sharedkey = "e9Bg1WvpYD27";
	var data;

	var paymentGetway = function(data){
		var currentDate = new Date();
		var month= currentDate.getMonth();
		var day = currentDate.getDate();
		var datetime;
		if(month>9){
			datetime = currentDate.getFullYear()+""+currentDate.getMonth();
		}else{
			datetime = currentDate.getFullYear()+"0"+currentDate.getMonth();
		}
		if(day>9){
			datetime =datetime+""+currentDate.getDate()+"000000";
		}else{
			datetime =datetime+"0"+currentDate.getDate()+"000000";
		}
		log.info(currentDate.getDate());
		log.info(currentDate.getMonth());
		log.info(currentDate.getFullYear());
		var WORDS = sha1 (data.price + mallId +sharedkey + data.transid); 
		var obj = {};
		obj.action = "http://staging.doku.com/Suite/Receive";
		obj.mallId = mallId;
		obj.amount=data.price;
		obj.transIdMarchant = data.transid;
		obj.words = WORDS;
		obj.sessionId = dbConfig.getRandomString();
		obj.dateTime = datetime;
		obj.name = data.name;
		obj.email = data.email;
		obj.basketName= data.basketName+","+data.price+",1,"+data.price;
		
		if(paymentmode == "A"){
			obj.paymentChannel = "15";
		}else if(paymentmode == "B"){
			obj.paymentChannel = "05";
		}else{
			obj.paymentChannel = "02";
		}

		log.info(obj);
		callback(null,obj);
	}

	pool.query(`SELECT umrohregister.*, umroh.name as basketName
		FROM "Hasanah".umrohregister
		INNER JOIN "Hasanah".umroh on umroh.id = umrohregister.typeid
		WHERE umrohregister.trans_id='`+orderId+`'`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				data = result.rows[0];
				paymentGetway(data);
			}
		});
};

module.exports = model;