var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var documentID = req.body.id;
	var imgUpdate = true;
	var pdfUpdate = true;

	var fileName = "";
	var pdfFileName = "";

	if (documentID)
	{
    	if (req.body.img.substr(0,14) == "uploads/umroh/") //if update and image not changed
    	{
    		imgUpdate = false;
    		fileName = req.body.img;
    		//saveData(req, req.body.img, req.body.pdf_file, callback);
    	}
    	if (req.body.pdf_file.length && req.body.pdf_file.substr(0,18) == "uploads/umroh/pdf/") //if update and file not changed
    	{
    		pdfUpdate = false;
    		pdfFileName = req.body.pdf_file;
			//saveData(req, req.body.img, req.body.pdf_file, callback);
		}

		if (imgUpdate)
		{
			fs.exists(req.body.old_img, function(exists) {
				if (exists) {
					fs.unlink(req.body.old_img)
				}
			});

			var imgBuffer =  Buffer.from(req.body.img, 'base64');
			sharp(imgBuffer)
			.resize(47, 47)
			.toBuffer()
			.then(data => {
				(async () => {
					data = await imagemin.buffer(data, {
						plugins: [
						imageminMozjpeg(),
						imageminPngquant({quality: '40-60'})
						]
					});

					data =  Buffer.from(data).toString('base64');

					fileName = "uploads/umroh/"+Date.now()+".png";

					fs.writeFile(fileName, data, 'base64', function(err) {
						if (err) {
							callback({status: "error", message: "Inputan tidak valid."}, null);
						}
					});

					if (pdfUpdate)
					{
						pdfFileName = "uploads/umroh/pdf/"+Date.now()+".pdf";

						fs.writeFile(pdfFileName, req.body.pdf_file.base64, 'base64', function(err) {
							if (err) {
								callback({status: "error", message: "Inputan tidak valid."}, null);
							}
						});

						saveData(req, fileName, pdfFileName, callback);
					}
					else
					{
						saveData(req, fileName, pdfFileName, callback);
					}

				})();
			})
			.catch(err => {
				callback({status: "error", message: "Inputan tidak invalid."}, null);
			});
		}
		else if(pdfUpdate)
		{
			fs.exists(req.body.old_pdf_file, function(exists) {
				if (exists) {
					fs.unlink(req.body.old_pdf_file)
				}
			});

			pdfFileName = "uploads/umroh/pdf/"+Date.now()+".pdf";

			fs.writeFile(pdfFileName, req.body.pdf_file.base64, 'base64', function(err) {
				if (err) {
					callback({status: "error", message: "Inputan tidak valid."}, null);
				}
			});

			saveData(req, fileName, pdfFileName, callback);
		}
		else
			saveData(req, fileName, pdfFileName, callback);	
	}
	else
	{
		var imgBuffer =  Buffer.from(req.body.img, 'base64');
		sharp(imgBuffer)
		.resize(47, 47)
		.toBuffer()
		.then(data => {
			(async () => {
				data = await imagemin.buffer(data, {
					plugins: [
					imageminMozjpeg(),
					imageminPngquant({quality: '40-60'})
					]
				});

				data =  Buffer.from(data).toString('base64');

				fileName = "uploads/umroh/"+Date.now()+".png";
             	   if (documentID) { //if update and image changed, keep old file name
             	   	fileName = req.body.old_img;
             	   }

             	   fs.writeFile(fileName, data, 'base64', function(err) {
             	   	if (err) {
             	   		callback({status: "error", message: "Inputan tidak valid."}, null);
             	   	}
             	   });

             	   pdfFileName = "uploads/umroh/pdf/"+Date.now()+".pdf";
                	if (documentID) { //if update and image changed, keep old file name
                		pdfFileName = req.body.old_img;
                	}

                	fs.writeFile(pdfFileName, req.body.pdf_file.base64, 'base64', function(err) {
                		if (err) {
                			callback({status: "error", message: "Inputan tidak valid."}, null);
                		}
                	});

                	saveData(req, fileName, pdfFileName, callback);
                })();
            })
		.catch(err => {
			callback({status: "error", message: "Inputan tidak invalid."}, null);
		});
	}

	function saveData(req, fileName, pdfFileName, callback)
	{
		var dates = [];
		for (let x=0;x<req.body.departure_date_list.length;x++) {
			dates.push(req.body.departure_date_list[x]['date']);
		}

		var id = req.body.id;

		if (id)  {
			pool.query(`UPDATE "Hasanah".umroh SET 
				travelagentid = `+dbConfig.checkUpdate(req.body.travelagent_id, 'travelagentid')+`,
				name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
				img = '`+fileName+`',
				city = `+dbConfig.checkUpdate(req.body.city, 'city')+`,
				departure = `+dbConfig.checkUpdate(req.body.departure, 'departure')+`,
				price = `+dbConfig.checkUpdate(req.body.price, 'price')+`,
				packet = `+dbConfig.checkUpdate(req.body.packet, 'packet')+`,
				plane = `+dbConfig.checkUpdate(req.body.plane, 'plane')+`,
				hotel = `+dbConfig.checkUpdate(req.body.hotel, 'hotel')+`,
				phone = `+dbConfig.checkUpdate(req.body.phone, 'phone')+`,
				quota = `+dbConfig.checkUpdate(req.body.quota, 'quota')+`,
				content = `+dbConfig.checkUpdate(req.body.content, 'content')+`,
				status = `+dbConfig.checkUpdate(req.body.status, 'status')+`,
				quad_price = `+dbConfig.checkUpdate(req.body.quad_price, 'quad_price')+`,
				triple_price = `+dbConfig.checkUpdate(req.body.triple_price, 'triple_price')+`,
				double_price = `+dbConfig.checkUpdate(req.body.double_price, 'double_price')+`,
				child_price = `+dbConfig.checkUpdate(req.body.child_price, 'child_price')+`,
				infant_price = `+dbConfig.checkUpdate(req.body.infant_price, 'infant_price')+`,
				mahram_price = `+dbConfig.checkUpdate(req.body.mahram_price, 'mahram_price')+`,
				handling_price = `+dbConfig.checkUpdate(req.body.handling_price, 'handling_price')+`,
				total_days = `+dbConfig.checkUpdate(req.body.total_days, 'total_days')+`,
				depart_routes = `+dbConfig.checkUpdate(req.body.depart_routes, 'depart_routes')+`,
				return_routes = `+dbConfig.checkUpdate(req.body.return_routes, 'return_routes')+`,
				mecca_hotel = `+dbConfig.checkUpdate(req.body.mecca_hotel, 'mecca_hotel')+`,
				mecca_hotelstar = `+dbConfig.checkUpdate(req.body.mecca_hotelstar, 'mecca_hotelstar')+`,
				mecca_hoteldistance = `+dbConfig.checkUpdate(req.body.mecca_hoteldistance, 'mecca_hoteldistance')+`,
				mecca_pilgrimage_list = `+dbConfig.checkUpdate(req.body.mecca_pilgrimage_list, 'mecca_pilgrimage_list')+`,
				madina_hotel = `+dbConfig.checkUpdate(req.body.madina_hotel, 'madina_hotel')+`,
				madina_hotelstar = `+dbConfig.checkUpdate(req.body.madina_hotelstar, 'madina_hotelstar')+`,
				madina_hoteldistance = `+dbConfig.checkUpdate(req.body.madina_hoteldistance, 'madina_hoteldistance')+`,
				madina_pilgrimage_list = `+dbConfig.checkUpdate(req.body.madina_pilgrimage_list, 'madina_pilgrimage_list')+`,
				pending_note = `+dbConfig.checkUpdate(req.body.pending_note, 'pending_note')+`,
				refund_note = `+dbConfig.checkUpdate(req.body.refund_note, 'refund_note')+`,
				pic_email = `+dbConfig.checkUpdate(req.body.pic_email, 'pic_email')+`,
				meal = `+dbConfig.checkUpdate(req.body.meal, 'meal')+`,
				last_submitdoc = `+dbConfig.checkUpdate(req.body.last_submitdoc, 'last_submitdoc')+`,
				muttawif = `+dbConfig.checkUpdate(req.body.muttawif, 'muttawif')+`,
				tour_leader = `+dbConfig.checkUpdate(req.body.tour_leader, 'tour_leader')+`,
				ustad_name = `+dbConfig.checkUpdate(req.body.ustad_name, 'ustad_name')+`,
				handling_list = `+dbConfig.checkUpdate(req.body.handling_list, 'handling_list')+`,
				pic_name = `+dbConfig.checkUpdate(req.body.pic_name, 'pic_name')+`,
				`+(req.body.departure_date_list ? `departure_date_list = '`+JSON.stringify(dates).replace('[', '{').replace(']', '}')+`', ` : "")+`
				pdf_file_name = '`+pdfFileName+`'
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah diubah.", id: id});
					}
				});
		} else {
			pool.query(`INSERT INTO "Hasanah".umroh 
				(travelagentid, name, img, city, departure,
				price, packet, plane, hotel, phone,
				quota, content, status, quad_price, triple_price,
				double_price, child_price, infant_price, mahram_price, handling_price,
				total_days, depart_routes, return_routes, mecca_hotel, mecca_hotelstar,
				mecca_hoteldistance, mecca_pilgrimage_list, madina_hotel, madina_hotelstar, madina_hoteldistance, 
				madina_pilgrimage_list, pending_note, refund_note, pic_email, meal, 
				last_submitdoc, muttawif, tour_leader, ustad_name, handling_list, 
				pic_name, departure_date_list, pdf_file_name) 
				VALUES(`+dbConfig.checkInsert(req.body.travelagent_id)+`, 
				`+dbConfig.checkInsert(req.body.name)+`, 
				'`+fileName+`',
				`+dbConfig.checkInsert(req.body.city)+`, 
				`+dbConfig.checkInsert(req.body.departure)+`, 
				`+dbConfig.checkInsert(req.body.price)+`, 
				`+dbConfig.checkInsert(req.body.packet)+`, 
				`+dbConfig.checkInsert(req.body.plane)+`, 
				`+dbConfig.checkInsert(req.body.hotel)+`, 
				`+dbConfig.checkInsert(req.body.phone)+`, 
				`+dbConfig.checkInsert(req.body.quota)+`, 
				`+dbConfig.checkInsert(req.body.content)+`, 
				`+dbConfig.checkInsert(req.body.status)+`, 
				`+dbConfig.checkInsert(req.body.quad_price)+`, 
				`+dbConfig.checkInsert(req.body.triple_price)+`, 
				`+dbConfig.checkInsert(req.body.double_price)+`, 
				`+dbConfig.checkInsert(req.body.child_price)+`, 
				`+dbConfig.checkInsert(req.body.infant_price)+`, 
				`+dbConfig.checkInsert(req.body.mahram_price)+`, 
				`+dbConfig.checkInsert(req.body.handling_price)+`, 
				`+dbConfig.checkInsert(req.body.total_days)+`, 
				`+dbConfig.checkInsert(req.body.depart_routes)+`, 
				`+dbConfig.checkInsert(req.body.return_routes)+`, 
				`+dbConfig.checkInsert(req.body.mecca_hotel)+`, 
				`+dbConfig.checkInsert(req.body.mecca_hotelstar)+`, 
				`+dbConfig.checkInsert(req.body.mecca_hoteldistance)+`, 
				`+dbConfig.checkInsert(req.body.mecca_pilgrimage_list)+`, 
				`+dbConfig.checkInsert(req.body.madina_hotel)+`, 
				`+dbConfig.checkInsert(req.body.madina_hotelstar)+`, 
				`+dbConfig.checkInsert(req.body.madina_hoteldistance)+`, 
				`+dbConfig.checkInsert(req.body.madina_pilgrimage_list)+`, 
				`+dbConfig.checkInsert(req.body.pending_note)+`, 
				`+dbConfig.checkInsert(req.body.refund_note)+`, 
				`+dbConfig.checkInsert(req.body.pic_email)+`, 
				`+dbConfig.checkInsert(req.body.meal)+`, 
				`+dbConfig.checkInsert(req.body.last_submitdoc)+`, 
				`+dbConfig.checkInsert(req.body.muttawif)+`, 
				`+dbConfig.checkInsert(req.body.tour_leader)+`, 
				`+dbConfig.checkInsert(req.body.ustad_name)+`, 
				`+dbConfig.checkInsert(req.body.handling_list)+`, 
				`+dbConfig.checkInsert(req.body.pic_name)+`, 
				`+(req.body.departure_date_list ? `'`+JSON.stringify(dates).replace('[', '{').replace(']', '}')+`'` : 'DEFAULT')+`, 
				'`+pdfFileName+`')`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah disimpan.", id: id});
					}
				});
		}
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;
	
	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".umroh 
			WHERE id = '`+id+`'`, (error, result) => {
				fs.exists(req.body.old_img, function(exists) {
					if (exists) {
						fs.unlink(req.body.old_img)
					}
				});

				fs.exists(req.body.old_pdf, function(exists) {
					if (exists) {
						fs.unlink(req.body.old_pdf)
					}
				});

				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.disableOldUmroh = function(callback) {
	pool.query(`UPDATE "Hasanah".umroh set status=0 WHERE departure < now() - interval '30 days'`, (error, result) => {
		if (error) {
			callback(error, null);
		} else {
			callback(null, result);
		}
	});
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".umroh 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	var status = req.body.status;
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var travelagent = req.body.travelagent_id;
	var filter = "";

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter = " AND status=" + status;
	}
	if (travelagent && travelagent !== "" && travelagent !== "undefined" && travelagent !== null ) {
		filter += " AND travelagentid='" + travelagent+"' ";
	}

	pool.query(`SELECT *
		FROM "Hasanah".umroh 
		WHERE 1=1 `+filter+`
		ORDER BY id
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getPlane = function(req, callback) {
	pool.query(`SELECT DISTINCT plane
		FROM "Hasanah".umroh 
		WHERE status=1
		ORDER BY plane`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getDepartureCity = function(req, callback) {
	pool.query(`SELECT DISTINCT city
		FROM "Hasanah".umroh 
		WHERE status=1
		ORDER BY city`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getActive = function(req, callback) {
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}

	pool.query(`SELECT *
		FROM "Hasanah".umroh 
		WHERE status=1
		ORDER BY id
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCount = function(req, callback) {
	var StartDate = new Date(req.body.date_start.split("-")[0], req.body.date_start.split("-")[1] - 1);
	var EndDate = new Date(req.body.date_end.split("-")[0], req.body.date_end.split("-")[1], 0);
	
	var city = req.body.city;
	var packet = req.body.packet;
	var plane = req.body.plane;
	var hotel = req.body.hotel;
	var dateStart = req.body.date_start;
	var dateEnd =  req.body.date_end;
	var minPrice = req.body.min_price;
	var maxPrice = req.body.max_price;
	var status = req.body.status;
	var travelagent = req.body.travelagent_id;
	var filter = "";

	if (travelagent && travelagent !== "" && travelagent !== "undefined") {
		filter += " AND travelagentid='" + travelagent +"'";
	}
	if (status) {
		filter += " AND status=" + status;
	}
	else {
		filter += " AND status=1";
	}
	if (city) {
		filter += " AND LOWER(city) LIKE '%" + city.toLowerCase() + "%'";
	}
	if (packet) {
		filter += " AND LOWER(packet)='" + packet.toLowerCase() + "'";
	}
	if (plane) {
		filter += " AND LOWER(plane)='" + plane.toLowerCase() + "'";
	}
	if (hotel) {
		filter += " AND hotel=" + hotel;
	}
	if (minPrice && maxPrice) {
		filter += " AND (quad_price>=" + minPrice + " AND quad_price<=" + maxPrice + ")";
	}
	if (dateStart && dateEnd) {
		filter += " AND departure BETWEEN '"+StartDate.format('yyyy-mm-dd')+"' AND '"+EndDate.format('yyyy-mm-dd')+"'";
	}

	console.log(`SELECT COUNT(1) as total
		FROM "Hasanah".umroh 
		WHERE 1=1 `+filter);
	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".umroh 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};


model.getCountAll = function(req, callback) {
	var StartDate = new Date(req.body.date_start.split("-")[0], req.body.date_start.split("-")[1] - 1);
	var EndDate = new Date(req.body.date_end.split("-")[0], req.body.date_end.split("-")[1], 0);
	
	var city = req.body.city;
	var packet = req.body.packet;
	var plane = req.body.plane;
	var hotel = req.body.hotel;
	var dateStart = req.body.date_start;
	var dateEnd =  req.body.date_end;
	var minPrice = req.body.min_price;
	var maxPrice = req.body.max_price;
	var status = req.body.status;
	var travelagent = req.body.travelagent_id;
	var filter = "";

	if (travelagent && travelagent !== "" && travelagent !== "undefined") {
		filter += " AND travelagentid='" + travelagent +"'";
	}
	if (status) {
		filter += " AND status=" + status;
	}
	if (city) {
		filter += " AND LOWER(city) LIKE '%" + city.toLowerCase() + "%'";
	}
	if (packet) {
		filter += " AND LOWER(packet)='" + packet.toLowerCase() + "'";
	}
	if (plane) {
		filter += " AND LOWER(plane)='" + plane.toLowerCase() + "'";
	}
	if (hotel) {
		filter += " AND hotel=" + hotel;
	}
	if (minPrice && maxPrice) {
		filter += " AND (quad_price>=" + minPrice + " AND quad_price<=" + maxPrice + ")";
	}
	if (dateStart && dateEnd) {
		filter += " AND departure BETWEEN '"+StartDate.format('yyyy-mm-dd')+"' AND '"+EndDate.format('yyyy-mm-dd')+"'";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".umroh 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};


model.search = function(req, callback) {
	var StartDate = new Date(req.body.date_start.split("-")[0], req.body.date_start.split("-")[1] - 1);
	var EndDate = new Date(req.body.date_end.split("-")[0], req.body.date_end.split("-")[1], 0);
	
	var city = req.body.city;
	var packet = req.body.packet;
	var plane = req.body.plane;
	var hotel = req.body.hotel;
	var dateStart = req.body.date_start;
	var dateEnd =  req.body.date_end;
	var minPrice = req.body.min_price;
	var maxPrice = req.body.max_price;
	var status = req.body.status;

	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;
	var filter = "";

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}
	if (status) {
		filter += " AND status=" + status;
	}
	else {
		filter += " AND status=1";
	}
	if (city) {
		filter += " AND LOWER(umroh.city) LIKE '%" + city.toLowerCase() + "%'";
	}
	if (packet) {
		filter += " AND LOWER(packet)='" + packet.toLowerCase() + "'";
	}
	if (plane) {
		filter += " AND LOWER(plane)='" + plane.toLowerCase() + "'";
	}
	if (hotel) {
		filter += " AND hotel=" + hotel;
	}
	if (minPrice) {
		filter += " AND (quad_price>=" + minPrice + ")";
	}
	if (maxPrice) {
		filter += " AND (quad_price<=" + maxPrice + ")";
	}
	if (dateStart && dateEnd) {
		filter += " AND departure BETWEEN '"+StartDate.format('yyyy-mm-dd')+"' AND '"+EndDate.format('yyyy-mm-dd')+"'";
	}

	pool.query(`SELECT umroh.id, umroh.name, travelagency.img, umroh.city, umroh.departure, umroh.quad_price as price, 
		umroh.quad_price, umroh.triple_price, umroh.double_price, umroh.handling_price, umroh.infant_price, 
		umroh.child_price, umroh.mahram_price, umroh.packet, umroh.phone, umroh.plane, umroh.hotel, umroh.quota, 
		umroh.content, umroh.status, umroh.travelagentid, travelagency.name as travel, umroh.total_days, 
		umroh.depart_routes, umroh.return_routes,  umroh.mecca_hotel, umroh.mecca_hotelstar, umroh.mecca_hoteldistance,  
		umroh.madina_hotel, umroh.madina_hotelstar,  umroh.madina_hoteldistance,  umroh.meal, umroh.mecca_pilgrimage_list, 
		umroh.madina_pilgrimage_list, umroh.last_submitdoc,  umroh.muttawif, umroh.tour_leader, umroh.ustad_name, 
		umroh.handling_list, umroh.pic_name, umroh.pending_note, umroh.refund_note, umroh.pic_email, umroh.pdf_file_name 
		FROM "Hasanah".umroh
		INNER JOIN "Hasanah".travelagency 
		ON travelagency.id = umroh.travelagentid
		WHERE 1=1 `+filter+`
		ORDER BY quad_price
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;