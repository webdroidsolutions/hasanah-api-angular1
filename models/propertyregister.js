var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
var mail = require('../util/mail.js');
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

var finalResult = [];

model.save = function(req, callback) {
	var id = req.body.id;
	var date = new Date().format('yyyy-mm-dd HH:MM:ss');

	if (id)  {
		pool.query(`UPDATE "Hasanah".propertyregister SET 
			date = CURRENT_TIMESTAMP,
			name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
			address = `+dbConfig.checkUpdate(req.body.address, 'address')+`,
			gender = `+dbConfig.checkUpdate(req.body.gender, 'gender')+`,
			phone = `+dbConfig.checkUpdate(req.body.phone, 'phone')+`,
			branchid = `+dbConfig.checkUpdate(req.body.nearest_branch_id, 'branchid')+`,
			typeid = `+dbConfig.checkUpdate(req.body.type_id, 'typeid')+`
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah diubah.", id: id});
				}
			});
	} else {
		mail.send("propertysetting", function(result) {});

		pool.query(`INSERT INTO "Hasanah".propertyregister 
			(date, name, address, gender, phone, branchid, typeid) 
			VALUES(CURRENT_TIMESTAMP, 
			`+dbConfig.checkInsert(req.body.name)+`, 
			`+dbConfig.checkInsert(req.body.address)+`, 
			`+dbConfig.checkInsert(req.body.gender)+`, 
			`+dbConfig.checkInsert(req.body.phone)+`, 
			`+dbConfig.checkInsert(req.body.nearest_branch_id)+`, 
			`+dbConfig.checkInsert(req.body.type_id)+`)`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
				}
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".propertyregister 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		finalResult = [];
		start();

		async function start() {
			await getPropertyRegister(" AND propertyregister.id = '"+id+"'");
			await startProperty();
			await startBranch();

			callback(null, finalResult);
		};
	}
};

model.getAll = function(req, callback) {
	finalResult = [];
	start();

	async function start() {
		await getPropertyRegister("");
		await startProperty();
		await startBranch();

		callback(null, finalResult);
	};
};

model.getReport = function(req, callback) {
	var dateStart = req.body.date_start;
	var dateEnd = req.body.date_end;

	var filter = "";
	if (dateStart && dateEnd) {
		filter += " AND propertyregister.date BETWEEN '" + dateStart + "' AND '" + dateEnd + "'";
	}

	finalResult = [];
	start();

	async function start() {
		await getPropertyRegister(filter);
		await startProperty();
		await startBranch();

		callback(null, finalResult);
	};
};

model.search = function(req, callback) {
	var key = req.body.key;

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		finalResult = [];
		start();

		async function start() {
			await getPropertyRegister(" AND LOWER(propertyregister.name) LIKE '%"+key.toLowerCase()+"%'");
			await startProperty();
			await startBranch();

			callback(null, finalResult);
		};
	}
};

async function getPropertyRegister(filter) {
	await pool.query(`SELECT propertyregister.*
		FROM "Hasanah".propertyregister 
		WHERE 1=1 `+filter+`
		ORDER BY propertyregister.id`)
	.then(result => {
		(async () => {
			for(var i = 0; i < result.rows.length; i++) {
				finalResult[i] = {}
				finalResult[i]['propertyregister'] = result.rows[i];
			}
		})();
	}, error => {
		callback(error, null);
	});
}

async function startProperty() {
	for(var i = 0; i < finalResult.length; i++) {
		await getProperty(i);
	}
}

async function getProperty(i) {
	await pool.query(`SELECT *
		FROM "Hasanah".property 
		WHERE id = `+finalResult[i]['propertyregister']['typeid']+`
		LIMIT 1`)
	.then(result => {
		(async () => {
			finalResult[i]['property'] = result.rows[0];
		})();
	}, error => {
		callback(error, null);
	});
}

async function startBranch() {
	for(var i = 0; i < finalResult.length; i++) {
		await getBranch(i);
	}
}

async function getBranch(i) {
	await pool.query(`SELECT *
		FROM "Hasanah".branch 
		WHERE id = `+finalResult[i]['propertyregister']['branchid']+`
		LIMIT 1`)
	.then(result => {
		(async () => {
			finalResult[i]['branch'] = result.rows[0];
		})();
	}, error => {
		callback(error, null);
	});
}

module.exports = model;