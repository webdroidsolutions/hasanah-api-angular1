var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
var mail = require('../util/mail.js');
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

rating_avg = null;

//TODO: umrohregister_id se obj.is_rated = true;
model.save = function (req, callback) {
	var id = req.body.id;

	if(!req.body.umrohregister_id){
		callback({status: "error", message: "umrohregister_id is missing."}, null);
		return;
	} 
	if (!req.body.customer_id) {
		callback({status: "error", message: "customer_id is missing"}, null);
		return;
	} else {
		if (id)  {
			pool.query(`UPDATE "Hasanah".ratingreview SET 
				travelagentid = `+dbConfig.checkUpdate(req.body.travelagent_id, 'travelagentid')+`,
				umrohid = `+dbConfig.checkUpdate(req.body.umroh_id, 'umrohid')+`,
				umrohregisterid = `+dbConfig.checkUpdate(req.body.umrohregister_id, 'umrohregisterid')+`,
				userdeviceid = `+dbConfig.checkUpdate(req.body.userdevice_id, 'userdeviceid')+`,
				customer_name = `+dbConfig.checkUpdate(req.body.customer_name, 'customer_name')+`,
				rating = `+dbConfig.checkUpdate(req.body.rating, 'rating')+`,
				review = `+dbConfig.checkUpdate(req.body.review, 'review')+`,
				create_date = CURRENT_TIMESTAMP
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah diubah.", id: id});
					}
				});
		} else {
			pool.query(`INSERT INTO "Hasanah".ratingreview 
				(travelagentid, umrohid, umrohregisterid, userdeviceid, customer_name, rating, review, create_date) 
				VALUES(`+dbConfig.checkInsert(req.body.travelagent_id)+`,
				`+dbConfig.checkInsert(req.body.umroh_id)+`,
				`+dbConfig.checkInsert(req.body.umrohregister_id)+`,
				`+dbConfig.checkInsert(req.body.userdevice_id)+`,
				`+dbConfig.checkInsert(req.body.customer_name)+`,
				`+dbConfig.checkInsert(req.body.rating)+`,
				`+dbConfig.checkInsert(req.body.review)+`,
				CURRENT_TIMESTAMP)`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Terimakasih sudah memberikan penilaian", id: id});
					}
				});
		}
	}
};


model.getByUmroh = function (req, callback) {
	var umroh_id = req.body.umroh_id;

	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;

	if (!limit) {
		limit = 5;
	}
	if (page && page > 0) {
		offset = (page - 1) * limit;
	}

	pool.query(`SELECT *
		FROM "Hasanah".ratingreview 
		WHERE umrohid= '`+umroh_id+`'
		ORDER BY create_date DESC
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				(async () => {
					rating_avg = null;
					start();

					async function start() {
						var data = result.rows;
						await getAvgRtg(umroh_id);
						data.rating_avg = rating_avg;
						callback(null, data);
					};
				})();
			}
		});
};

async function getAvgRtg(umroh_id) {
	await pool.query(`SELECT avg(rating) as rating_avg
		FROM "Hasanah".ratingreview
		WHERE umrohid= '`+umroh_id+`'
		GROUP BY umrohid`)
	.then(result => {
		(async () => {
			rating_avg = result.rows[0].rating_avg;
		})();
	}, error => {
		callback(error, null);
	});
}

model.getByCustomer = function (req, callback) {
	var customer_id = req.body.userdevice_id;
	var travel_id = req.body.travelagent_id;

	pool.query(`SELECT message.*, travelagency.name
		FROM "Hasanah".message 
		LEFT JOIN "Hasanah".travelagency ON travelagency.id = message.travelagentid
		WHERE message.userdeviceid = '`+customer_id+`'
		ORDER BY message.create_date DESC`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCount = function(req, callback) {
	var umroh_id = req.body.umroh_id;
	var filter = "";

	if (umroh_id) {
		filter += " AND umrohid='" + umroh_id +"';";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".ratingreview 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getAll = function (req, callback) {
	var travelagent = req.body.travelagent_id;
	var filter = "";

	if (travelagent && travelagent !== "" && travelagent !== "undefined") {
		filter += " AND travelagentid='" + travelagent + "'";
	}

	pool.query(`SELECT *
		FROM "Hasanah".message 
		WHERE 1=1 `+filter+`
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.delete = function (req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".ratingreview 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

module.exports = model;