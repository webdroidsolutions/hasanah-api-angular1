var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var id = req.body.id;

	if (id)  {
		pool.query(`UPDATE "Hasanah".city SET 
			name = `+dbConfig.checkUpdate(req.body.name, 'name')+`
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah diubah.", id: id});
				}
			});
	} else {
		pool.query(`INSERT INTO "Hasanah".city 
			(name) 
			VALUES(`+dbConfig.checkInsert(req.body.name)+`)`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
				}
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".city 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".city 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	var limit = req.body.limit;
	var page = req.body.page;
	var offset = 0;

	if (!limit) {
		limit = 10;
	}
	if (page && page>0) {
		offset = (page - 1) * limit;
	}

	pool.query(`SELECT *
		FROM "Hasanah".city 
		ORDER BY name
		LIMIT `+limit+` OFFSET `+offset, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getAllCity = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".city 
		ORDER BY name`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getCount = function(req, callback) {
	var key = req.body.key;
	var filter = "";

	if (key) {
		filter += " AND LOWER(name) LIKE '%" + key.toLowerCase() + "%'";
	}

	pool.query(`SELECT COUNT(1) as total
		FROM "Hasanah".city 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;
	var filter = "";

	if (key) {
		filter += " AND LOWER(name) LIKE '%" + key.toLowerCase() + "%'";
	}

	pool.query(`SELECT *
		FROM "Hasanah".city 
		WHERE 1=1 `+filter+`
		ORDER BY name`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;