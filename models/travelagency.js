var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";
var sharp = require('sharp');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

function model(docType) {
	documentType = docType;
};

async function insertFileName(value) {
	fileNames.push(value);
}

async function resetFileName(value) {
	fileNames = [];
}

var firstFileName = null;
var fileNames = [];
var x=0;

model.saveAgency = function(req, callback) {
	var documentID = req.body.id;

	if (!req.body.name || req.body.name=='') {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		if (!documentID) {
			pool.query(`SELECT *
				FROM "Hasanah".travelagency 
				WHERE LOWER(name) = '`+req.body.name.toLowerCase()+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						if (result.rows.length > 0) {
							callback({status: "success", message: "Travel Agency Name telah terdaftar."}, null);
							return;
						}
					}
				});
		}

		function saveImage(img, fileName) {
			var imgBuffer =  Buffer.from(img, 'base64');
			sharp(imgBuffer)
			//.resize(360, 240)
			.toBuffer()
			.then(data => {
				(async () => {
					data = await imagemin.buffer(data, {
						plugins: [
						imageminMozjpeg(),
						imageminPngquant({quality: '40-60'})
						]
					});

					data =  Buffer.from(data).toString('base64');

					fs.writeFile(fileName, data, 'base64', function(err) {
						if (err) {
							callback({status: "error", message: "Inputan tidak valid."}, null);
							return;
						} 
					});
				})();
			})
			.catch(err => {
				callback({status: "error", message: "Inputan tidak invalid."}, null);
			});
		}

		async function uploadImage(x) {
			var fileName = "uploads/travelagency/portfolio/"+Date.now()+".png";
			if (req.body.portfolio_img[x].substr(0,31) == "uploads/travelagency/portfolio/") //if update and image not changed
			{
				fileName = req.body.portfolio_img[x];
			}
			await insertFileName(fileName);

			if (req.body.portfolio_img[x].substr(0,31) != "uploads/travelagency/portfolio/")
				await saveImage(req.body.portfolio_img[x], fileName);
		};

		async function startImageUpload() {
			for (let x=0;x<req.body.portfolio_img.length;x++) {
				await uploadImage(x);
			}
		};

		
		async function startUpload() {
			await resetFileName();
			await startImageUpload();

			var id = req.body.id;

        	if (req.body.img.substr(0,21) == "uploads/travelagency/") //if update and image not changed
        	{
        		saveData(req, req.body.img, callback);
        	}
        	else
        	{
	            if (id) { //if update and image changed, delete old image
	            	fs.exists(req.body.old_img, function(exists) {
	            		if (exists) {
	            			fs.unlink(req.body.old_img)
	            		}
	            	});
	            }

	            var imgBuffer =  Buffer.from(req.body.img, 'base64');
	            sharp(imgBuffer)
	            //.resize(55, 55)
	            .toBuffer()
	            .then(data => {
	            	(async () => {
	            		data = await imagemin.buffer(data, {
	            			plugins: [
	            			imageminMozjpeg(),
	            			imageminPngquant({quality: '40-60'})
	            			]
	            		});

	            		data =  Buffer.from(data).toString('base64');

	            		var fileName = "uploads/travelagency/"+Date.now()+".png";
                    	if (documentID) { //if update and image changed, keep old file name
                    		fileName = req.body.old_img;
                    	}

                    	fs.writeFile(fileName, data, 'base64', function(err) {
                    		if (err) {
                    			callback({status: "error", message: "Inputan tidak valid."}, null);
                    		}
                    	});

                    	saveData(req, fileName, callback);
                    })();
                })
	            .catch(err => {
	            	callback({status: "error", message: err+"Inputan tidak invalid."}, null);
	            });
	        }	        
	    };

	    startUpload();

	    function saveData(req, fileName, callback) {
	    	var id = req.body.id;

	    	if (id)  {
	    		pool.query(`UPDATE "Hasanah".travelagency SET 
	    			name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
	    			province = `+dbConfig.checkUpdate(req.body.province, 'province')+`,
	    			city = `+dbConfig.checkUpdate(req.body.city, 'city')+`,
	    			district = `+dbConfig.checkUpdate(req.body.district, 'district')+`,
	    			zone = `+dbConfig.checkUpdate(req.body.zone, 'zone')+`,
	    			address = `+dbConfig.checkUpdate(req.body.address, 'address')+`,
	    			pic_name = `+dbConfig.checkUpdate(req.body.pic_name, 'pic_name')+`,
	    			pic_hp = `+dbConfig.checkUpdate(req.body.pic_hp, 'pic_hp')+`,
	    			umroh_license = `+dbConfig.checkUpdate(req.body.umroh_license, 'umroh_license')+`,
	    			hajj_license = `+dbConfig.checkUpdate(req.body.hajj_license, 'hajj_license')+`,
	    			travel_license = `+dbConfig.checkUpdate(req.body.travel_license, 'travel_license')+`,
	    			asita_no = `+dbConfig.checkUpdate(req.body.asita_no, 'asita_no')+`,
	    			iata_no = `+dbConfig.checkUpdate(req.body.iata_no, 'iata_no')+`,
	    			association = `+dbConfig.checkUpdate(req.body.association, 'association')+`,
	    			agreement = `+dbConfig.checkUpdate(req.body.agreement, 'agreement')+`,
	    			agreement_start_date = `+dbConfig.checkUpdate(req.body.agreement_start_date, 'agreement_start_date')+`,
	    			agreement_end_date = `+dbConfig.checkUpdate(req.body.agreement_end_date, 'agreement_end_date')+`,
	    			img = '`+fileName+`',
	    			portfolio_img = '`+JSON.stringify(fileNames).replace('[', '{').replace(']', '}')+`'
	    			WHERE id = '`+id+`'`, (error, result) => {
	    				if (error) {
	    					callback(error, null);
	    				} else {
	    					callback(null, {status: "success", message: "Data telah diubah.", id: id});
	    				}
	    			});
	    	} else {
	    		pool.query(`INSERT INTO "Hasanah".travelagency 
	    			(name, province, city, district, zone, address, pic_name, pic_hp, umroh_license, hajj_license, travel_license, 
	    			asita_no, iata_no, association, agreement, agreement_start_date, agreement_end_date, img, portfolio_img) 
	    			VALUES(`+dbConfig.checkInsert(req.body.name)+`,
	    			`+dbConfig.checkInsert(req.body.province)+`,
	    			`+dbConfig.checkInsert(req.body.city)+`,
	    			`+dbConfig.checkInsert(req.body.district)+`,
	    			`+dbConfig.checkInsert(req.body.zone)+`,
	    			`+dbConfig.checkInsert(req.body.address)+`,
	    			`+dbConfig.checkInsert(req.body.pic_name)+`,
	    			`+dbConfig.checkInsert(req.body.pic_hp)+`,
	    			`+dbConfig.checkInsert(req.body.umroh_license)+`,
	    			`+dbConfig.checkInsert(req.body.hajj_license)+`,
	    			`+dbConfig.checkInsert(req.body.travel_license)+`,
	    			`+dbConfig.checkInsert(req.body.asita_no)+`,
	    			`+dbConfig.checkInsert(req.body.iata_no)+`,
	    			`+dbConfig.checkInsert(req.body.association)+`,
	    			`+dbConfig.checkInsert(req.body.agreement)+`,
	    			`+dbConfig.checkInsert(req.body.agreement_start_date)+`,
	    			`+dbConfig.checkInsert(req.body.agreement_end_date)+`,
	    			'`+fileName+`', 
	    			'`+JSON.stringify(fileNames).replace('[', '{').replace(']', '}')+`')`, (error, result) => {
	    				if (error) {
	    					callback(error, null);
	    				} else {
	    					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
	    				}
	    			});
	    	}
	    }
	};
};

model.getAll = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".travelagency 
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getByName = function(req, callback) {
	if (!req.body.name || req.body.name=='') {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	}

	pool.query(`SELECT *
		FROM "Hasanah".travelagency 
		WHERE LOWER(name) like '%`+req.body.name.toLowerCase()+`%'
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.delete = function(req, callback) {
	var id = req.body.id;

	async function deleteImage(img) {
		await fs.exists(img, function(exists) {
			if (exists) {
				(async () => {
					await fs.unlink(img)
				})();
			}
		});
	};

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		(async () => {
			await deleteImage(req.body.img);

			for(var img of req.body.portfolio_img) {
				await deleteImage(img)
			}

			pool.query(`DELETE FROM "Hasanah".travelagency 
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah dihapus."});
					}
				});
		})();
	}
};

model.getProvince = function(req, callback){
	var type = req.body.type;
	var filter = "";

	if(type == 'province'){
		filter = "";
	}else if(type == 'regency'){
		filter = " AND province_id="+ req.body.provinceId;
	}else if(type == 'district'){
		filter = " AND regency_id="+ req.body.regencyId;
	}else if(type == 'village'){
		filter = " AND district_id="+ req.body.districtId;
	}

	pool.query(`SELECT id, `+type+`
		FROM "Hasanah".`+type+` 
		WHERE 1=1 `+filter, (error, result) => {
			if (error) {
				callback(error+"112312", null);
			} else {
				callback(null, result.rows);
			}
		});
}

module.exports = model;