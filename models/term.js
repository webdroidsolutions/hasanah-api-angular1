var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var id = req.body.id;

	if (id)  {
		pool.query(`UPDATE "Hasanah".term SET 
			name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
			content = `+dbConfig.checkUpdate(req.body.content, 'content')+`
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah diubah.", id: id});
				}
			});
	} else {
		pool.query(`INSERT INTO "Hasanah".term 
			(name, content) 
			VALUES(name = `+dbConfig.checkUpdate(req.body.name, 'name')+`,
			content = `+dbConfig.checkUpdate(req.body.content, 'content')+`)`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah disimpan.", id: id});
				}
			});
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".term 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".term 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".term 
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.search = function(req, callback) {
	var key = req.body.key;

	if (!key) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".term
			WHERE LOWER(name) LIKE '%` + key.toLowerCase() + `%'
			ORDER BY id`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows);
				}
			});
	}
};

module.exports = model;