var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var pool = dbConfig.getPostgreDb();
var documentType = "";

function model(docType) {
	documentType = docType;
};

model.save = function(req, callback) {
	var id = req.body.id;
	var description = req.body.description;

	if (!description) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		if (id)  {
			pool.query(`UPDATE "Hasanah".calculator SET 
				typeCalculator = `+dbConfig.checkUpdate(req.body.typeCalculator, 'typeCalculator')+`, 
				typeOfWork = `+dbConfig.checkUpdate(req.body.typeOfWork, 'typeOfWork')+`, 
				percentage = `+dbConfig.checkUpdate(req.body.percentage, 'percentage')+`
				WHERE id = '`+id+`'`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah diubah.", id: id});
					}
				});
		} else {
			pool.query(`INSERT INTO "Hasanah".calculator 
				(typeCalculator, typeOfWork, percentage) 
				VALUES(`+dbConfig.checkInsert(req.body.typeCalculator)+`, 
				`+dbConfig.checkInsert(req.body.typeOfWork)+`, 
				`+dbConfig.checkInsert(req.body.percentage)+`)`, (error, result) => {
					if (error) {
						callback(error, null);
					} else {
						callback(null, {status: "success", message: "Data telah disimpan.", id: id});
					}
				});
		}
	}
};

model.delete = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`DELETE FROM "Hasanah".calculator 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, {status: "success", message: "Data telah dihapus."});
				}
			});
	}
};

model.getById = function(req, callback) {
	var id = req.body.id;

	if (!id) {
		callback({status: "error", message: "Inputan tidak valid."}, null);
	} else {
		pool.query(`SELECT *
			FROM "Hasanah".calculator 
			WHERE id = '`+id+`'`, (error, result) => {
				if (error) {
					callback(error, null);
				} else {
					callback(null, result.rows[0]);
				}
			});
	}
};

model.getAll = function(req, callback) {
	pool.query(`SELECT *
		FROM "Hasanah".calculator 
		ORDER BY typeCalculator`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getByType = function(req, callback) {
	var type = req.body.type;
	var filter = "";

	if (type) {
		type = type.toLowerCase();
		filter = " AND LOWER(typeCalculator)='" + type + "'";
	}

	pool.query(`SELECT *
		FROM "Hasanah".calculator 
		WHERE 1=1 `+filter+`
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

model.getByTypeAndWork = function(req, callback) {
	var type = req.body.type;
	var work = req.body.work;
	var filter = "";

	if (type && work) {
		filter = " AND LOWER(typeCalculator)='" + type.toLowerCase() + "' AND LOWER(typeOfWork)='" + work.toLowerCase() + "'";
	}

	pool.query(`SELECT *
		FROM "Hasanah".calculator 
		WHERE 1=1 `+filter+`
		ORDER BY id`, (error, result) => {
			if (error) {
				callback(error, null);
			} else {
				callback(null, result.rows);
			}
		});
};

module.exports = model;